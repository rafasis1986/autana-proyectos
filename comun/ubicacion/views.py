# -*- coding: utf-8 -*-

from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

from comun.mensajes import MENSAJES_UBICACION
from ubicacion.models import Estado, Municipio, Parroquia
from ubicacion.serializers import EstadoSerializer, MunicipioSerializer,\
    ParroquiaSerializer

@api_view(('GET',))
def api_root(request, format=None):
    """
    El punto de entrada para el API.
    """
    
    return Response({
        'estados': reverse('estado-list', request=request),
    })

class EstadoList(generics.ListAPIView):
    """
    Representa la lista de estados.
    """

    model = Estado
    serializer_class = EstadoSerializer

class EstadoDetail(generics.RetrieveAPIView):
    """
    Representa un estado.
    """

    model = Estado
    serializer_class = EstadoSerializer

class MunicipioList(generics.ListAPIView):
    """
    Representa la lista de municipios.
    """

    model = Municipio
    serializer_class = MunicipioSerializer

    def get_queryset(self):
        """
        Debe retornar la lista de los municipios para el estado
        determinado por el id_estado del url.
        """
        
        id_estado = self.kwargs['id_estado']
        return Municipio.objects.filter(estado__id=id_estado)

class ParroquiaList(generics.ListAPIView):
    """
    Representa la lista de parroquias.
    """

    model = Parroquia
    serializer_class = ParroquiaSerializer

    def get_queryset(self):
        """
        Debe retornar la lista de las parroquias para el municipio
        determinado por el id_municipio del url.
        """
        
        id_municipio = self.kwargs['id_municipio']
        return Parroquia.objects.filter(municipio__id=id_municipio)

class ParroquiaDetail(generics.RetrieveAPIView):
    """
    Representa una parroquia.
    """

    model = Parroquia
    serializer_class = ParroquiaSerializer

@api_view(['GET'])
def municipio_por_parroquia(request, id_parroquia):
    """
    Permite obtener el municipio asociado a una parroquia.
    
    @param id_parroquia: El identificador de la parroquia.
    @type  id_parroquia: entero
    
    @return: El municipio asociado a la parroquia
    """

    try:
        parroquia = Parroquia.objects.get(pk=id_parroquia)
    except Parroquia.DoesNotExist:
        error = {"error": MENSAJES_UBICACION['PARROQUIA_NO_EXISTE']}
        return Response(error)
        
    municipio = Municipio.objects.get(pk=parroquia.municipio.id)
    serializer = MunicipioSerializer(municipio)
    return Response(serializer.data)

@api_view(['GET'])
def estado_por_municipio(request, id_municipio):
    """
    Permite obtener el estado asociado a un municipio.
    
    @param id_municipio: El identificador del municipio.
    @type  id_municipio: entero
    
    @return: El estado asociado al municipio
    """

    try:
        municipio = Municipio.objects.get(pk=id_municipio)
    except Municipio.DoesNotExist:
        error = {"error": MENSAJES_UBICACION['MUNICIPIO_NO_EXISTE']}
        return Response(error)
        
    estado = Estado.objects.get(pk=municipio.estado.id)
    serializer = EstadoSerializer(estado)
    return Response(serializer.data)
