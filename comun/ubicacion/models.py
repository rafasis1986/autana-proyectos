# -*- coding: utf-8 -*-

from django.db import models

class Estado(models.Model):
    """
    Modelo del estado
    """
    
    nombre = models.CharField(max_length=50)
    
    class Meta:
        ordering = ('nombre',)
        
    def __unicode__(self):
        return self.nombre

class Municipio(models.Model):
    """
    Modelo del municipio
    """

    estado = models.ForeignKey(Estado)
    nombre = models.CharField(max_length=50)
    
    class Meta:
        ordering = ('nombre',)
        
    def __unicode__(self):
        return self.nombre

class Parroquia(models.Model):
    """
    Modelo de la parroquia
    """

    municipio = models.ForeignKey(Municipio)
    nombre = models.CharField(max_length=50)
    
    class Meta:
        ordering = ('nombre',)
        
    def __unicode__(self):
        return self.nombre