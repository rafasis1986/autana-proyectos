# -*- coding: utf-8 -*-

from rest_framework import serializers

from ubicacion.models import Estado, Municipio, Parroquia

class EstadoSerializer(serializers.ModelSerializer):
    """
    Clase que permite serializar los datos del estado
    """
    
    class Meta:
        model = Estado
        fields = ('id', 'nombre',)

class MunicipioSerializer(serializers.ModelSerializer):
    """
    Clase que permite serializar los datos del municipio
    """

    class Meta:
        model = Municipio
        fields = ('id', 'nombre',)

class ParroquiaSerializer(serializers.ModelSerializer):
    """
    Clase que permite serializar los datos de la parroquia
    """

    class Meta:
        model = Parroquia
        fields = ('id', 'nombre',)
