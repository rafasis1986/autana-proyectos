#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from ubicacion import views

urlpatterns = patterns('ubicacion.views',
    url(r'^$', 'api_root'),
    url(r'^estados/$', views.EstadoList.as_view(), name='estado-list'),
    url(r'^estados/(?P<pk>[0-9]+)/$', views.EstadoDetail.as_view(), name='estado-detail'),
    url(r'^municipios/(?P<id_estado>.+)/$', views.MunicipioList.as_view(), name='municipio-list'),
    url(r'^parroquias/(?P<id_municipio>.+)/$', views.ParroquiaList.as_view(), name='parroquia-list'),
    url(r'^parroquia/(?P<pk>[0-9]+)/$', views.ParroquiaDetail.as_view(), name='parroquia-detail'),
    url(r'^municipio_por_parroquia/(?P<id_parroquia>\d+)/$', 'municipio_por_parroquia'),
    url(r'^estado_por_municipio/(?P<id_municipio>\d+)/$', 'estado_por_municipio'),
)

urlpatterns = format_suffix_patterns(urlpatterns)