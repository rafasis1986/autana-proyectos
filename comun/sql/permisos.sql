INSERT INTO auth_group (id, name) VALUES (101, 'operadora');
INSERT INTO auth_group (id, name) VALUES (102, 'fidetel_recaudacion');


INSERT INTO django_content_type (id, name, app_label, model) VALUES (101, 'recaudacion operadora', 'recaudacion_operadora', 'operadora');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (102, 'recaudacion usuario operadora', 'recaudacion_usuario_operadora', 'usuariooperadora');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (103, 'recaudacion pago', 'recaudacion_pago', 'pago');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (104, 'recaudacion convenio', 'recaudacion_convenio', 'convenio');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (105, 'recaudacion estado de cuenta', 'recaudacion_estado_cuenta', 'estadocuenta');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (106, 'recaudacion solvencia', 'recaudacion_solvencia', 'solvencia');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (107, 'recaudacion declaracion', 'recaudacion_declaracion', 'declaracion');
    
INSERT INTO django_content_type (id, name, app_label, model) VALUES (108, 'recaudacion usuario fidetel', 'recaudacion_usuario_fidetel', 'usuariofidetel');


INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (101, 'Crear Operadora', 101, 'crear_operadora');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (102, 'Consultar Operadora', 101, 'consultar_operadora');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (103, 'Modificar Operadora', 101, 'modificar_operadora');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (104, 'Crear Usuario Operadora', 102, 'crear_usuario_operadora');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (105, 'Consultar Usuario Operadora', 102, 'consultar_usuario_operadora');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (106, 'Modificar Usuario Operadora', 102, 'modificar_usuario_operadora');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (107, 'Realizar Pago', 103, 'realizar_pago');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (109, 'Leer Estado de Cuenta', 105, 'leer_estado_cuenta');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (110, 'Generar Referencia de Pago', 106, 'leer_solvencia');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (111, 'Realizar Declaración', 107, 'realizar_declarcion');

INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (112, 'Crear Usuario Fidetel', 108, 'crear_usuario_fidetel');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (113, 'Consultar Usuario Fidetel', 108, 'consultar_usuario_fidetel');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (114, 'Modificar Usuario Fidetel', 108, 'modificar_usuario_fidetel');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (115, 'Registrar Convenio', 104, 'registrar_convenio');

INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (116, 'Consultar Convenio', 104, 'consultar_convenio');


INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (101, 101, 101);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (102, 101, 102);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (103, 101, 103);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (104, 101, 104);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (105, 101, 105);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (106, 101, 106);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (107, 101, 107);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (109, 101, 109);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (110, 101, 110);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (111, 101, 111);

INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (116, 101, 116);

INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (112, 102, 112);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (113, 102, 113);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (114, 102, 114);
INSERT INTO auth_group_permissions (id, group_id, permission_id) VALUES (115, 102, 115);
