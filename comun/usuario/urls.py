#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from usuario import views

urlpatterns = patterns('usuario.views',
    url(r'^usuarios/$', views.UserList.as_view(), name='usuario-list'),
    url(r'^usuarios/(?P<pk>[0-9]+)/$', views.UserInstance.as_view(), name='usuario-detail'),
    url(r'^usuario/$', 'usuario_autenticacion'),
    url(r'^usuario/operadora/$', 'usuario_autenticacion_operadora'),
    url(r'^usuario/restablecer/$', 'usuario_restablecer'),
    url(r'^usuario/ingresar/contrasena/$', 'ingresar_contrasena'),
    url(r'^usuario/modificar/contrasena/$', 'modificar_contrasena'),
    url(r'^usuario/crear/operadora/$', 'crear_operadora'),
    url(r'^usuario/fidetel/$', 'usuario_autenticacion_fidetel'),
    url(r'^usuario/crear/$', 'crear_usuario'),
)

urlpatterns = format_suffix_patterns(urlpatterns)