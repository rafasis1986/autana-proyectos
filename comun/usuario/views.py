# -*- coding: utf-8 -*-

from django.db import transaction
from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.hashers import UNUSABLE_PASSWORD
from django.contrib.auth.models import User, Group
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import base36_to_int

from datetime import datetime
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response

from comun.mensajes import MENSAJES_USUARIO
from usuario.constantes import TIPO_USUARIO
from usuario.models import TipoUsuario
from usuario.serializers import UserSerializer

class UserList(generics.ListAPIView):
    """
    Representa la lista de usuarios.
    """

    model = User
    serializer_class = UserSerializer

class UserInstance(generics.RetrieveAPIView):
    """
    Representa un usuario.
    """

    model = User
    serializer_class = UserSerializer

@api_view(['POST', ])
def usuario_autenticacion(request):
    """
    Permite autenticar un usuario operadora y validar si se encuentra activo

    @return: El usuario autenticado o un mensaje de error en el caso de que el
        usuario no exista
    """

    if request.method == 'POST':
        username = request.POST.get('usuario')
        password = request.POST.get('contrasena')

        return Response(validar_usuario_tipo(username, password))

@api_view(['POST', ])
def usuario_autenticacion_operadora(request):
    """
    Permite autenticar un usuario operadora y validar si se encuentra activo

    @return: El usuario autenticado o un mensaje de error en el caso de que el
        usuario no exista
    """

    if request.method == 'POST':
        username = request.POST.get('usuario')
        password = request.POST.get('contrasena')

        return Response(validar_usuario_tipo(username, password, TIPO_USUARIO['OPERADORA']))

@api_view(['POST', ])
def usuario_autenticacion_fidetel(request):
    """
    Permite autenticar un usuario fidetel y validar si se encuentra activo

    @return: El usuario autenticado o un mensaje de error en el caso de que el
        usuario no exista
    """

    if request.method == 'POST':
        username = request.POST.get('usuario')
        password = request.POST.get('contrasena')

        return Response(validar_usuario_tipo(username, password, TIPO_USUARIO['FIDETEL']))

@api_view(['GET', 'POST', ])
def usuario_restablecer(request):
    """
    Permite validar que el correo electrónico se encuentra asociado a un
    usuario dado y que el usuario se encuentra activo.

    Si es llamado por POST valida que el token dado es correcto y retorna el
    usuario asociado

    @return: El usuario validado
    """

    # El método GET valida que el correo se encuentra asociado al usuario
    if request.method == 'GET':
        username = request.GET.get('usuario')
        correo = request.GET.get('correo')

        users = User.objects.filter(username=username,
                                    email__iexact=correo,
                                    is_active=True)

        if not len(users):
            error = {"error": MENSAJES_USUARIO['RESTABLECER_CORREO_NO_ASOCIADO'] }
            return Response(error)

        if any((user.password == UNUSABLE_PASSWORD) for user in users):
            error = {"error": MENSAJES_USUARIO['RESTABLECER_UNUSABLE_PASSWORD']}
            return Response(error)

        serializer = UserSerializer(users[0])
        serializer.data['token'] = default_token_generator.make_token(users[0]);
        return Response(serializer.data)

    # El método POST el token para el usuario dado
    if request.method == 'POST':

        try:
            uidb36 = request.POST.get('uidb36')
            uid_int = base36_to_int(uidb36)
            token = request.POST.get('token')

            user = User.objects.get(id=uid_int)

        except (ValueError, User.DoesNotExist):
            user = None

        if user is not None and default_token_generator.check_token(user, token):
            serializer = UserSerializer(user)
            return Response(serializer.data)
        else:
            error = {"error": MENSAJES_USUARIO['RESTABLECER_ENLACE_NO_VALIDO'] }
            return Response(error)

@api_view(['POST', ])
def modificar_contrasena(request):
    """
    Permite modificar la contraseña del usuario.

    @return: El usuario al que se le ha modificado la contraseña
    """

    if request.method == 'POST':
        uid = request.POST.get('uid')
        contrasena = request.POST.get('contrasena')

        user = User.objects.get(id=uid)

        if user is not None:
            user.set_password(contrasena)
            user.last_login = datetime.now()
            user.save()

            serializer = UserSerializer(user)
            serializer.data['permisos'] = user.get_all_permissions();
            return Response(serializer.data)
        else:
            error = {"error": MENSAJES_USUARIO['VALIDAR_USUARIO_NO_VALIDO']}
            return Response(error)

@api_view(['POST', ])
def ingresar_contrasena(request, grupo=None):
    """
    Permite ingresar la contraseña del usuario.

    @return: El usuario al que se le ha ingresado la contraseña
    """

    if request.method == 'POST':
        uid = request.POST.get('uid')
        contrasena = request.POST.get('contrasena')
        grupo = request.POST.get('grupo')
        if grupo is None:
            grupo = 'GRUPO_OPERADORA'

        user = User.objects.get(id=uid)

        if user is not None:
            user.set_password(contrasena)
            user.last_login = datetime.now()
            user.is_active = True
            user.save()

            grupo_operadora = Group.objects.get(name=settings.NOMBRE_GRUPOS[grupo])
            grupo_operadora.user_set.add(user)
            grupo_operadora.save()

            serializer = UserSerializer(user)
            serializer.data['permisos'] = user.get_all_permissions();
            return Response(serializer.data)
        else:
            error = {"error": MENSAJES_USUARIO['VALIDAR_USUARIO_NO_VALIDO']}
            return Response(error)

@api_view(['POST', ])
@transaction.commit_on_success
def crear_operadora(request):
    """
    Permite crear un usuario asociado a una operadora asignandole los permisos
    asociados al mismo

    @return: El usuario creado con los permisos asociados
    """

    if request.method == 'POST':
        username = request.POST.get('usuario')
        correo = request.POST.get('correo')

        if usuario_existe(username):
            error = {"error": MENSAJES_USUARIO['OPERADORA_CREAR_ERROR']}
            return Response(error)

        user = User.objects.create_user(username, correo, '')
        user.tipo_usuario = TipoUsuario.objects.all().get(pk=TIPO_USUARIO['OPERADORA'])
        user.is_active = False
        user.save()

        serializer = UserSerializer(user)
        serializer.data['token'] = default_token_generator.make_token(user);
        return Response(serializer.data)

def usuario_existe(username, app=None):
    """
    Valida si el usuario ya existe en la base de datos

    @param username: nombre de usuario.
    @type  username: cadena

    @return: Verdadero en cado de que el usuario exista
    """
    try:
       usuario = User.objects.get(username=username)
       if not app is None:
           pass

    except User.DoesNotExist:
        return False
    return True

def validar_usuario_tipo(username, password, tipo=None):
    """
    Permite validar que un usuario existe, está activo y pertenece al tipo dado

    @param username: Nombre de usuario
    @param password: Contraseña del usuario
    @param tipo: Tipo de usuario

    @return: El usuario en caso de ser válido o un mensaje de error en formato JSON
    """
    user = authenticate(username=username, password=password)
    if user is not None and user.tipo_usuario is not None and (tipo == None or user.tipo_usuario.id == tipo):
        serializer = UserSerializer(user)
        if user.is_active:
            serializer.data['permisos'] = user.get_all_permissions();
            return serializer.data
        else:
            error = {"error" : MENSAJES_USUARIO['VALIDAR_USUARIO_INACTIVO'] }
            return error
    else:
        error = {"error": MENSAJES_USUARIO['VALIDAR_USUARIO_NO_VALIDO']}
        return error


@api_view(['POST', ])
@transaction.commit_on_success
def crear_usuario(request):
    """
    Permite crear un usuario asociado a una operadora asignandole los permisos
    asociados al mismo

    @return: El usuario creado con los permisos asociados
    """
    respuesta = None
    user = None
    if request.method == 'POST':
        username = request.POST.get('usuario')
        correo = request.POST.get('correo')
        app = request.POST.get('app')
        user = None
        if usuario_existe(username):
            id_app = TIPO_USUARIO[app]
            user = User.objects.get(username=username)
            if usuario_en_app(user.apps_usuario, id_app):
                error = {"error": MENSAJES_USUARIO['USUARIO_CREAR_EXISTE'] % app}
                respuesta = Response(error)
            else:
                user.apps_usuario += id_app
                user.save(force_update=True)
        else:
            user = User.objects.create_user(username, correo, '')
            user.tipo_usuario = TipoUsuario.objects.get(nombre=app.lower())
            user.apps_usuario = 0 + TIPO_USUARIO[app]
            user.is_active = False
            user.save()
        if respuesta is None and not user is None:
            serializer = UserSerializer(user)
            serializer.data['token'] = default_token_generator.make_token(user);
            respuesta = Response(serializer.data)
        return respuesta

def usuario_en_app(tipo_usuario, cod_app):
    '''
    Funcion que verifica si un usuario existe para una app
    '''
    respuesta = False
    if isinstance(tipo_usuario, int) and isinstance(cod_app, int):
        if len(bin(cod_app)) <= len(bin(tipo_usuario)):
            if bin(tipo_usuario)[2:][-len(bin(cod_app)[2:0])] == '1':
                respuesta = True
    return respuesta
