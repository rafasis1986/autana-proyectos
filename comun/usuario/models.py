#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

class TipoUsuario(models.Model):
    """
    Modelo que representa una profesion en el usuario fidetel
    """

    nombre = models.CharField(max_length=255, null=False)
    descripcion = models.CharField(max_length=500, null=True)


User.add_to_class('tipo_usuario', models.ForeignKey(TipoUsuario, null=True))
'''
Atributo que contendra las aplicaciones en las cuales puede loguearse el usuario,
para tal efecto se le dara a cada app un numero equivalente a 2 elevado al numero de app,
para la creacion de este atributo existian 2 apps y se agrego proyectos, por lo tanto el id de
PROYECTOS es 2^3=4=100(BINARIO), de este modo cada vez que se desee consultar si un usuario tiene
acceso a alguna app solo hay que transformar el valor de apps_usuario a binario y consultar la casilla
referente al bit de la aplicacion, en caso de que el valor del atributo sea menor al de la app en 
cuestion, pues se asume que aun no se ha registrado al usuario para la misma 
'''
User.add_to_class('apps_usuario', models.IntegerField('', default=0))
