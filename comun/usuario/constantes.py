# -*- coding: utf-8 -*-

# Tipos de usuario en recaudación
TIPO_USUARIO = {
    'OPERADORA': 1,
    'FIDETEL': 2,
    'PROYECTOS': 4,
}
