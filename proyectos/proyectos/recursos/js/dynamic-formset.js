function updateElementIndex(el, prefix, ndx) {
	var id_regex = new RegExp('(' + prefix + '-\\d+)');
	var replacement = prefix + '-' + ndx;
	if ($(el).attr("for")) $(el).attr("for", $(el).attr("for").replace(id_regex, replacement));
	if (el.id) el.id = el.id.replace(id_regex, replacement);
	if (el.name) el.name = el.name.replace(id_regex, replacement);
}

function addForm(btn, prefix) {
    var formCount = parseInt($('#id_' + prefix + '-TOTAL_FORMS').val());
    hideAdd(prefix);
    $('.dynamic-form-'+prefix+':first').find('.asyncError').each(function() {
    	$(this).remove();
    });
    var row = $('.dynamic-form-'+prefix+':first').clone(true).get(0);
    $(row).removeAttr('id').insertAfter($('.dynamic-form-'+prefix+':last')).children('.hidden').removeClass('hidden');
    $(row).children().not(':last').children().each(function() {
        updateElementIndex(this, prefix, formCount);
   	    $(this).val('');
    });
    $('#id_' + prefix + '-' + formCount + '-id').removeAttr('value');
    $('#id_' + prefix + '-TOTAL_FORMS').val(formCount + 1);
    
    addAllFormSetValidation(prefix)
    
    return false;
}

function deleteForm(btn, prefix) {
	if($(btn).parents('.dynamic-form-'+prefix).attr('id') !== undefined)
	{
		$(btn).parents('.dynamic-form-'+prefix).find('[id$="DELETE"]').attr('checked', true);
		$(btn).parents('.dynamic-form-'+prefix).hide();
	}
	else
	{
	    $(btn).parents('.dynamic-form-'+prefix).remove();
	    var forms = $('.dynamic-form-'+prefix);
	    $('#id_' + prefix + '-TOTAL_FORMS').val(forms.length);
	    for (var i=0, formCount=forms.length; i<formCount; i++) {
		    $(forms.get(i)).children().not(':last').children().each(function() {
		        updateElementIndex(this, prefix, i);
		    });
	    }		
	}
    showLastAdd(prefix);
    addAllFormSetValidation(prefix)
    return false;
}

function hideAdd(prefix)
{
    var forms = $('.dynamic-form-'+prefix);
    for (var i=0, formCount=forms.length; i<formCount; i++) {
	    $(forms.get(i)).children(':last').each(function() {
	        $(this).addClass('hidden');
	    });
    }
    return false;
}

function showLastAdd(prefix)
{
	hideAdd(prefix);
    $('.dynamic-form-'+prefix+':visible(:last)').children(':last').removeClass('hidden');
    return false;
}


function addFormContacto(btn, prefix) {
    var formCount = parseInt($('#id_' + prefix + '-TOTAL_FORMS').val());
    hideAddContacto(prefix);
    $('.dynamic-form-'+prefix+':first').find('.asyncError').each(function() {
    	$(this).remove();
    });
    var row = $('.dynamic-form-'+prefix+':first').clone(true).get(0);
    $(row).removeAttr('id').insertAfter($('.dynamic-form-'+prefix+':last')).children().children().children().children(':last').children().children('.hidden').removeClass('hidden');
    $(row).children().children().children().children().not(':last').each(function() {
    	$(this).children('td:last').children().each(function() {
	        updateElementIndexContacto(this, prefix, formCount);
	   	    if($(this).is('table'))
	   	    {
	   	    	$(this).children().children('[class^="dynamic-form-"]').not(':first').each(function() {
	   	    		$(this).remove();
	   	    	});
	   	    	updateTableElementIndexContacto(this, prefix, formCount);
	   	    	$(this).children().children('[class^="dynamic-form-"]').each(function() {
	   	    		$(this).children(':last').removeClass('hidden');
	   	    		$(this).children().children().each(function() {
	   	    			if($(this).attr('id'))
	   	    			{
	   	    				if($(this).attr('id').match(/-id$/))
	   	    				{
					    		$(this).removeAttr('value');
	   	    				}
	   	    				else if($(this).attr('id').match(/-numero$/))
	   	    				{
				   	    		$(this).val('');
					    		$(this).attr('value', '');
	   	    				}
			   	    	}
	   	    		});
	   	    	});
	   	    	$(this).find('[id$="-TOTAL_FORMS"]').val('1');
	   	    	$(this).find('[id$="-INITIAL_FORMS"]').val('0');
	   	    }
	   	    else
	   	    {
	   	    	$(this).val('');
	   	    }
   	    });
    });
    $('#id_' + prefix + '-' + formCount + '-id').removeAttr('value');
    $('#id_' + prefix + '-TOTAL_FORMS').val(formCount + 1);
    
    addAllFormSetValidation(prefix)
    
    return false;
}

function hideAddContacto(prefix)
{
    var forms = $('.dynamic-form-'+prefix);
    for (var i=0, formCount=forms.length; i<formCount; i++) {
	    $(forms.get(i)).children().children().find('tr:last').find('a:first').each(function() {
	        $(this).addClass('hidden');
	    });
    }
    return false;
}

function showLastAddContacto(prefix)
{
	hideAddContacto(prefix);
    $('.dynamic-form-'+prefix+':visible:last').children().children().find('tr:last').find('a:first').removeClass('hidden');
    return false;
}

function updateElementIndexContacto(el, prefix, ndx) {
	var id_regex = new RegExp('(' + prefix + '-\\d+)');
	var replacement = prefix + '-' + ndx;
	if ($(el).attr("for")) $(el).attr("for", $(el).attr("for").replace(id_regex, replacement));
	if (el.id) el.id = el.id.replace(id_regex, replacement);
	if (el.name) el.name = el.name.replace(id_regex, replacement);
}

function updateTableElementIndexContacto(el, prefix, ndx) {
	var id_regex = new RegExp('(' + prefix + '_\\d+)', 'g');
	var replacement = prefix + '_' + ndx;
	$(el).html($(el).html().replace(id_regex, replacement));
}

function deleteFormContacto(btn, prefix) {
	if($(btn).parents('.dynamic-form-'+prefix).attr('id') !== undefined)
	{
		$(btn).parents('.dynamic-form-'+prefix).children().children().children().children(':first').find('[id$="DELETE"]').attr('checked', true);
		$(btn).parents('.dynamic-form-'+prefix).hide();
	}
	else
	{
	    $(btn).parents('.dynamic-form-'+prefix).remove();
	    var forms = $('.dynamic-form-'+prefix);
	    $('#id_' + prefix + '-TOTAL_FORMS').val(forms.length);
	    for (var i=0, formCount=forms.length; i<formCount; i++) {
		    $(forms.get(i)).each(function() {
		    	updateRowIndexContacto(this, prefix, i);
		    });
	    }		
	}
    showLastAddContacto(prefix);
    addAllFormSetValidation(prefix)
    return false;
}

function updateRowIndexContacto(el, prefix, ndx)
{
    $(el).children().children().children().children().not(':last').each(function() {
    	$(this).children('td:last').children().each(function() {
	        updateElementIndexContacto(this, prefix, ndx);
	   	    if($(this).is('table'))
	   	    {
	   	    	updateTableElementIndexContacto(this, prefix, ndx);
	   	    }
   	    });
    });
}
