$(function() {
	$( "#tabsFormFichaNatural" ).tabs();
	
	
	/**
	 * Funcion para obtener el numero de decimales deseado
	 */
	Number.prototype.decimal = function(n) {
	    pot = Math.pow(10, parseInt(n));
	    return Math.round(this * pot) / pot;
	};

	/**
	 * Objeto javascript principal para encapsular rutinas comunes
	 */
	UTIL = {

	    vdigito : function(vDigito)
	    {
	        regx = /^\d+$/;
	        return regx.test(vDigito);
	    },

	    vmonto : function(vMonto)
	    {
	        regx = /^\d{1,12}(\,\d{1,2})?$/;
	        return regx.test(vMonto);
	    },
	    
	    vurl : function(vURL)
	    {
	        regx = /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/;
	        return regx.test(vURL);
	    },

	    vfecha : function(vFecha)
	    {
	        regx = /^((^(3[01]|[12][0-9]|0?[1-9])([\/])(10|12|0?[13578])([\/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(30|[12][0-9]|0?[1-9])([\/])(11|0?[469])([\/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(2[0-8]|1[0-9]|0?[1-9])([\/])(0?2)([\/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(29)([\/])(0?2)([\/])([2468][048]00)$)|(^(29)([\/])(0?2)([\/])([3579][26]00)$)|(^(29)([\/])(0?2)([\/])([1][89][0][48])$)|(^(29)([\/])(0?2)([\/])([2-9][0-9][0][48])$)|(^(29)([\/])(0?2)([\/])([1][89][2468][048])$)|(^(29)([\/])(0?2)([\/])([2-9][0-9][2468][048])$)|(^(29)([\/])(0?2)([\/])([1][89][13579][26])$)|(^(29)([\/])(0?2)([\/])([2-9][0-9][13579][26])$))$/;
	        return regx.test(vFecha);
	    },
	    
	    insertarError : function(error, idAfter)
	    {
	        error.wrap('<span class="asyncError" style="position: absolute; margin-top:5px;"></span>').parent().insertAfter("#"+idAfter);
	    },

	    insertarErrorElemento : function(error, elemento)
	    {
	        error.wrap('<span class="asyncError" style="position: absolute; margin-top:5px;"></span>').parent().insertAfter(elemento);
	    },
	    
	    comparar_fechas : function(fecha1, fecha2)
	    {
	        var fecha1_arr = fecha1.split("/");
	        var fecha1_date = new Date(Number(fecha1_arr[2]), Number(fecha1_arr[1]) - 1, Number(fecha1_arr[0]));
	        var fecha2_arr = fecha2.split("/");
	        var fecha2_date = new Date(Number(fecha2_arr[2]), Number(fecha2_arr[1]) - 1, Number(fecha2_arr[0]));
	        return fecha1_date < fecha2_date?-1:(fecha1_date == fecha2_date?0:1);
	    },

	    fecha_antes_hoy : function(fecha)
	    {
	        var fecha_arr = fecha.split("/");
	        var fecha_date = new Date(Number(fecha_arr[2]), Number(fecha_arr[1]) - 1, Number(fecha_arr[0]));
	        return fecha_date <= new Date();
	    }

	};
	
});