$(function (){
	$('#scorebarBorder').hide();
	
	$("#form_confirmar").validate({
		onkeyup: false,
		rules: {
			contrasena: {
				required: true,
				minlength : 6,
			},
			conf_contrasena: {
				required: true,
				equalTo: "#id_contrasena",
			},
			pregunta_seguridad: "required",
	        respuesta_seguridad: "required",
		},
		errorPlacement: function(error, element) {
			UTIL.insertarErrorElemento(error, element);
	  	},
		messages: {
			contrasena:	{
				required: "Este campo es obligatorio",
				minlength: "La contraseña debe tener al menos 6 caracteres"
			},
			pregunta_seguridad: "Este campo es obligatorio",
	        respuesta_seguridad: "Este campo es obligatorio",
			conf_contrasena: {
				required: "Este campo es obligatorio",
				equalTo: "Debe ser igual a la contraseña"
			}
		}
	});
	$('#id_contrasena').keyup(function(){
		$(this).val()!=""?$('#scorebarBorder').show():$('#scorebarBorder').hide();
		chkPass($(this).val())
	});
});