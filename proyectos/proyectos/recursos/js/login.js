$(document).ready(function (){
    $("#form_login").validate({
        onkeyup: false,
        rules: {
            tipo_per: {
                required: true,
            },
            rif: {
                required: true,
                digits: true,
                minlength : 8
            },
            rif_fin: {
                required: true,
                digits: true
            },
            contrasena:	{
            	required: true,
            	minlength : 6
            },
            captcha_1: "required"
        },
        groups: {
            RIFOperadora: "tipo_per rif rif_fin"
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "tipo_per" || element.attr("name") == "rif" || element.attr("name") == "rif_fin") 
                UTIL.insertarError(error, 'id_rif_fin');
            else
                UTIL.insertarErrorElemento(error, element);
        },
        messages: {
            tipo_per: {
                required: "Este campo es obligatorio",
            },
            rif: {
                required: "Este campo es obligatorio",
                digits: "Este campo es numérico",
                minlength: "Este campo debe contener 8 dígitos"
            },
            rif_fin: {
                required: "Este campo es obligatorio",
                digits: "Este campo es numérico"
            },
            contrasena:{
            	reuired: "Este campo es obligatorio",
            	minlength: "La contraseña debe tener al menos 6 caracteres"
            },
            captcha_1: "Este campo es obligatorio"
        }
    });
    $("#loading").dialog({
        autoOpen: false,
        height: 100,
        width: 200,
        modal: true,
        closeOnEscape: false,
        draggable: false,
        resizable: false,
        dialogClass: "no-titlebar",
    });
    
    $(document).ajaxStart(function() {
        $("#loading").dialog("open");
    }).ajaxStop(function() {
        $("#loading").dialog("close");
    });
});
    
    
