$(function (){
	
	$("#form_login").validate({
	    onkeyup: false,
	    rules: {
	        nombre: "required",
	        apellido: "required",
	        tipo_per: "required",
	        rif: "required",
	        rif_fin: "required",
	        pregunta_seguridad: "required",
	        respuesta_seguridad: "required",
	        rif: {
	            required: true,
	            digits: true,
	            minlength : 8
	        },
	        correo: {
	            required: true,
	            email: true
	        },
	        rif_fin: {
	            required: true,
	            digits: true,
	            minlength : 1
	        },
	        captcha_1: "required"
	    },
	    groups: {
	        RIFSolicitante: "tipo_per rif rif_fin"
	    },
	    errorPlacement: function(error, element) {
	        if (element.attr("name") == "tipo_per" || element.attr("name") == "rif" || element.attr("name") == "rif_fin") 
	            UTIL.insertarError(error, 'id_rif_fin');
	        else
	            UTIL.insertarErrorElemento(error, element);
	    },
	    messages: {
	        nombre: "Este campo es obligatorio",
	        apellido: "Este campo es obligatorio",
	        tipo_rif: "Este campo es obligatorio",
	        rif: "Este campo es obligatorio",
	        rif_fin: "Este campo es obligatorio",
	        pregunta_seguridad: "Este campo es obligatorio",
	        respuesta_seguridad: "Este campo es obligatorio",
	        correo: {
	            required: "Este campo es obligatorio",
	            email: "Debe ser un correo electrónico válido"
	        },
	        rif_fin: {
	        	required: "Falta el ultimo digito del rif",
	            digits: "El rif es numérico",
	        	minlength: "Rif incompleto"
	        },
	        rif: {
	        	required: "Falta el número de rif",
	            digits: "El rif es numérico",
	        	minlength: "Rif incompleto"
	        },
	        	
	        captcha_1: "Este campo es obligatorio"
	    }
	});
});
        
        