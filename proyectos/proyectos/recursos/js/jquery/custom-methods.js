$(document).ready(
		function() {
			jQuery.validator.addMethod("distinctOf", 
				function(value, element, valueToCompare) 
				{
					return value != valueToCompare;
				},
				"Debe seleccionar un valor");

			jQuery.validator.addMethod("amount", 
				function(value, element) 
				{
					var regExPattern = /^\d{1,12}(\,\d{1,2})?$/;
					return this.optional(element) || value.match(regExPattern);
				},
				"Valor no válido");

			jQuery.validator.addMethod("percent", 
				function(value, element) 
				{
					var regExPattern = /^\d{1,3}(\,\d{1,2})?$/;
					return this.optional(element) || value.match(regExPattern);
				},
				"Valor no válido");

			jQuery.validator.addMethod("phoneVE", 
				function(value, element) 
				{
					var regExPattern = /^[2,4]\d{2}(-)?\d{7}$/;
					return this.optional(element) || value.match(regExPattern);
				},
				"El formato del teléfono no es válido");

			jQuery.validator.addMethod("validarRif",
				function(value,	element) 
				{
					var regExPattern = /^[EGIJPV][-]\d{8}[-]\d{1}$/;
					return this.optional(element) || value.match(regExPattern);
				},
				"La formato del RIF no es válido");

			jQuery.validator.addMethod("multipleOf",
				function(value,	element, valueToCompare) 
				{
					return value%valueToCompare == 0;
				},
				"El valor debe ser multiplo");

			jQuery.validator.addMethod("dateVE",
				function(value,	element) 
				{
					var regExPattern = /^((^(3[01]|[12][0-9]|0?[1-9])([\/])(10|12|0?[13578])([\/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(30|[12][0-9]|0?[1-9])([\/])(11|0?[469])([\/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(2[0-8]|1[0-9]|0?[1-9])([\/])(0?2)([\/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(29)([\/])(0?2)([\/])([2468][048]00)$)|(^(29)([\/])(0?2)([\/])([3579][26]00)$)|(^(29)([\/])(0?2)([\/])([1][89][0][48])$)|(^(29)([\/])(0?2)([\/])([2-9][0-9][0][48])$)|(^(29)([\/])(0?2)([\/])([1][89][2468][048])$)|(^(29)([\/])(0?2)([\/])([2-9][0-9][2468][048])$)|(^(29)([\/])(0?2)([\/])([1][89][13579][26])$)|(^(29)([\/])(0?2)([\/])([2-9][0-9][13579][26])$))$/;
					return this.optional(element) || value.match(regExPattern);
				},
				"La fecha no es válida");

			jQuery.validator.addMethod("dateBeforeToday",
				function(value,	element) 
				{
					var parts = value.split("/");
					var compareDate = new Date(Number(parts[2]), Number(parts[1]) - 1, Number(parts[0]));
					return this.optional(element) || compareDate <= new Date();
				},
				"La fecha debe ser anterior a la fecha actual");

			jQuery.validator.addMethod("dateBefore",
				function(value,	element, valueToCompare) 
				{
					var from = value.split("/");
					var fromDate = new Date(Number(from[2]), Number(from[1]) - 1, Number(from[0]));
					var to = $(valueToCompare).val().split("/");
					if(to.length == 3)
					{
						var toDate = new Date(Number(to[2]), Number(to[1]) - 1, Number(to[0]));
						return this.optional(element) || fromDate <= toDate;
					}
					return true;
				},
				"La fecha no es válida");

			jQuery.validator.addMethod("dateAfter",
				function(value,	element, valueToCompare) 
				{
					var to = value.split("/");
					var toDate = new Date(Number(to[2]), Number(to[1]) - 1, Number(to[0]));
					var from = $(valueToCompare).val().split("/");
					if(from.length == 3)
					{
						var fromDate = new Date(Number(from[2]), Number(from[1]) - 1, Number(from[0]));
						return this.optional(element) || fromDate <= toDate;
					}
					return true;
				},
				"La fecha no es válida");
});