#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import patterns, include, url

from proyectos_app.urls import proyectos_app_paterns
from comun.urls import comun_patterns


urlpatterns = patterns('',
    url(r'^recursos/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^comun/', include(comun_patterns)),
    url(r'^captcha/', include('captcha.urls')),
    ) + proyectos_app_paterns
