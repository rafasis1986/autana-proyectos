# -*- coding: utf-8 -*-

from django.conf import settings


class SessionExpiry(object):
    """ 
    Establece el tiempo de expiración de la sesión acorde con settings 
    """

    def process_request(self, request):
        if getattr(settings, 'SESSION_EXPIRY', None):
            request.session.set_expiry(settings.SESSION_EXPIRY)
        return None
