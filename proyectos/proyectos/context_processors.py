# -*- coding: utf-8 -*-
from django.conf import settings

from comun.constantes import PERMISOS_PROYECTOS


def permisos_proyectos(request):
    """
    Establece las variables asociadas a los permisos del archivo de configuración
    en el contexto del template
    """
    return PERMISOS_PROYECTOS


def base_url(request):
    """
    Agrega una variable de contexto que proporciona el url base configurado para
    la aplicación.
    """
    return {'BASE_URL': settings.BASE_URL}
