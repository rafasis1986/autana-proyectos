#!/usr/bin/env python
# -*- coding: utf-8 -*-

def verificarRif(nrorif):
    """
    @note: Función que permite verificar si el dígito que valida el RIF es el correcto
    @organization: CENDITEL nodo Mérida
    @author: T.S.U. Roldan D. Vargas G.
    @contact: rvargas@cenditel.gob.ve
    @return: Retorna Falso si el digito validador es incorrecto y Verdadero si es el correcto
    """
    if not nrorif or nrorif.__len__() < 10:
        return False
    elif nrorif.__len__() == 10:
        suma = 0
        divisor = 11
        resto = 0
        tiporif = 0
        digitoValidado = 0

        if nrorif[0:1] == "V" or nrorif[0:1] == "E" or nrorif[0:1] == "J" or nrorif[0:1] == "P" or nrorif[0:1] == "G":
            if nrorif[0:1] == "V":
                tiporif = 1
            elif nrorif[0:1] == "E":
                tiporif = 2
            elif nrorif[0:1] == "J":
                tiporif = 3
            elif nrorif[0:1] == "P":
                tiporif = 4
            elif nrorif[0:1] == "G":
                tiporif = 5

            suma = ((tiporif * 4) + (int(nrorif[1:2]) * 3) + (int(nrorif[2:3]) * 2)
                    + (int(nrorif[3:4]) * 7) + (int(nrorif[4:5]) * 6) + (int(nrorif[5:6]) * 5)
                    + (int(nrorif[6:7]) * 4) + (int(nrorif[7:8]) * 3) + (int(nrorif[8:9]) * 2))

            resto = suma % divisor

            digitoValidado = divisor - resto

        if int(digitoValidado) >= 10:
            digitoValidado = 0

        if int(nrorif[9:10]) == int(digitoValidado):
            return True
        else:
            return False
