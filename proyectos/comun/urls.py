#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url


urlpatterns = patterns('comun.views',
    url(r'^refrescar_captcha/$', 'refrescar_captcha', name='refrescar_captcha'),
    url(r'^confirmar/$', 'confirmar', name='confirmar'),
    url(r'^restablecer/$', 'restablecer_contrasena', name='restablecer_contrasena'),
    url(r'^municipios/$', 'municipios', name='obtener_municipios'),
    url(r'^parroquias/$', 'parroquias', name='obtener_parroquias'),
)

comun_patterns = urlpatterns
