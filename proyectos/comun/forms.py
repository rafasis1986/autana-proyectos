# -*- coding: utf-8 -*-

from django.forms.fields import RegexField, EMPTY_VALUES, CharField

from comun.aplicacion_comun import validar_usuario
import floppyforms as forms


class FormularioPersona(forms.ModelForm):
    """
    Clase que contiene el formulario que permite ingresar y modificar los datos
    básicos de las personas
    """
    nombre = forms.CharField(required=True, max_length=255,
        error_messages={'required': 'El Nombre es un campo obligatorio'})
    apellido = forms.CharField(required=True, max_length=255,
        error_messages={'required': 'El Apellido es un campo obligatorio'})
    correo = forms.EmailField(required=True, max_length=254,
        error_messages={'required': 'El Correo Electrónico es un campo obligatorio',
                'invalid': 'Ingrese un Correo Electrónico válido'})

    class Meta:
        abstract = True
        widgets = {
            'nombre': forms.TextInput,
            'apellido': forms.TimeInput,
            'correo': forms.EmailInput
            }

    def __init__(self, *args, **kwargs):
        super(FormularioPersona, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs = {'maxlength': '255'}
        self.fields['apellido'].widget.attrs = {'maxlength': '255'}
        self.fields['correo'].widget.attrs = {'maxlength': '254'}


class FormularioModificarContrasena(forms.Form):
    """
    Clase que contiene el formulario que permite modificar la contraseña
    """

    contrasena = CharField(widget=forms.PasswordInput(attrs={'style': 'width: 90px',
        'class': 'flotadoi'}), required=True)
    conf_contrasena = CharField(widget=forms.PasswordInput(attrs={'style': 'width: 90px'}),
        required=True)

    def clean_conf_contrasena(self):
        """
        Método que valida que la contraseña y la confirmacion de la contraseña
        sean iguales

        @return: Un mensaje de error si la contraseña y su confirmación no son
            iguales
        """
        contrasena = self.cleaned_data.get('contrasena')
        conf_contrasena = self.cleaned_data.get('conf_contrasena')
        if contrasena and conf_contrasena:
            if contrasena != conf_contrasena:
                raise forms.ValidationError('La contraseña no coincide')
        return conf_contrasena


class FormularioCambioContrasena(forms.Form):
    """
    Clase que contiene el formulario que permite cambiar la contraseña
    """
    contrasena = CharField(widget=forms.PasswordInput(attrs={'style': 'width: 90px'}),
                           required=True)
    nueva_contrasena = CharField(widget=forms.PasswordInput(attrs={'style': 'width: 90px',
                        'class': 'flotadoi'}), required=True)
    conf_contrasena = CharField(widget=forms.PasswordInput(attrs={'style': 'width: 90px'}),
                        required=True)

    def __init__(self, usuario=None, *args, **kwargs):
        super(FormularioCambioContrasena, self).__init__(*args, **kwargs)
        if usuario:
            self.usuario = usuario

    def clean_contrasena(self):
        """
        Método que valida que la contraseña es válida

        @return: Un mensaje de error si la contraseña no es válida
        """
        contrasena = self.cleaned_data.get('contrasena')
        if self.usuario:
            usuario = validar_usuario(self.usuario, contrasena)

            # Si el usuario no existe, la constraseña no es válida o no está activo
            if('error' in usuario):
                raise forms.ValidationError('La contraseña no es válida')

        else:
            raise forms.ValidationError('La contraseña no es válida')
        return contrasena

    def clean_conf_contrasena(self):
        """
        Método que valida que la contraseña y la confirmacion de la contraseña
        sean iguales

        @return: Un mensaje de error si la contraseña y su confirmación no son
            iguales
        """
        nueva_contrasena = self.cleaned_data.get('nueva_contrasena')
        conf_contrasena = self.cleaned_data.get('conf_contrasena')
        if nueva_contrasena and conf_contrasena:
            if nueva_contrasena != conf_contrasena:
                raise forms.ValidationError('La contraseña no coincide')
        return conf_contrasena


class VEPhoneNumberField(RegexField):
    """
    Venezuelan phone number. Ten digits with an optional hyphen or space after
    the first three digits.
    """
    default_error_messages = {
        'invalid': u'Los números telefónicos deben tener el formato ' +
                    u'2XX-XXXXXXX o 4XX-XXXXXXX' +
                    u' (solo números).',
        'min_length': u'Debe contener al menos 10 caracteres. '
    }

    def __init__(self, *args, **kwargs):
        kwargs['min_length'], kwargs['max_length'] = 10, 11
        super(VEPhoneNumberField, self).__init__(r'^[2,4]\d{2}(-)?\d{7}$', *args, **kwargs)

    def clean(self, value):
        value = super(VEPhoneNumberField, self).clean(value)

        if value in EMPTY_VALUES:
            return u''

        return value.replace(' ', '-')
