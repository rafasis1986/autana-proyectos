#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf import settings
import requests

from comun.constantes import APP_NAME
import json


def validar_usuario(nombre_usuario, contrasena):
    """
    Llamada al servicio REST en la aplicación comun que permite
    validar que el usuario solicitante existe y consultar la información asociada
    """
    parametros = {'usuario': nombre_usuario, 'contrasena': contrasena}
    r = requests.post(settings.APP_COMUN_URL + 'usuario/usuario/',
                      data=parametros, verify=settings.AUTANA_SSL_VERIFY)
    return json.loads(r.content)


def nuevo_usuario_solicitante_proyectos(nombre_usuario, correo):
    """
    Llamada al servicio REST en la aplicación comun que permite crear
    un nuevo usuario para la aplicacion de proyectos
    """
    parametros = {'usuario': nombre_usuario, 'correo': correo, 'app': APP_NAME}
    r = requests.post(settings.APP_COMUN_URL + 'usuario/usuario/crear/',
                      data=parametros, verify=settings.AUTANA_SSL_VERIFY)
    return json.loads(r.content)


def modificar_contrasena(id_usuario, contrasena):
    """
    Llamada al servicio REST en la aplicación comun que permite
    modificar la contraseña del usuario
    """

    parametros = {'uid': id_usuario,
                  'contrasena': contrasena}
    r = requests.post(settings.APP_COMUN_URL + 'usuario/usuario/modificar/contrasena/',
                      data=parametros, verify=settings.AUTANA_SSL_VERIFY)

    return json.loads(r.content)


def validar_usuario_token(uidb36, token):
    """
    Llamada al servicio REST en la aplicación comun que permite validar
    el usuario y token obtendio por URL
    """

    parametros = {'uidb36': uidb36,
                  'token': token}
    r = requests.post(settings.APP_COMUN_URL + 'usuario/usuario/restablecer/',
                      data=parametros, verify=settings.AUTANA_SSL_VERIFY)

    return json.loads(r.content)


def restablecer_contrasena(nombre_usuario, correo):
    """
    Llamada al servicio REST en la aplicación comun que permite restablecer
    la contraseña del usuario
    """

    parametros = {'usuario': nombre_usuario, 'correo': correo}
    r = requests.get(settings.APP_COMUN_URL + 'usuario/usuario/restablecer/',
                     params=parametros, verify=settings.AUTANA_SSL_VERIFY)

    return json.loads(r.content)


def usuario_ingresar_contrasena_grupo(id_usuario, contrasena, grupo):
    """
    Llamada al servicio REST en la aplicación comun que permite
    ingresar la contraseña del usuario
    """
    parametros = {'uid': id_usuario, 'contrasena': contrasena, 'grupo': grupo}
    r = requests.post(settings.APP_COMUN_URL + 'usuario/usuario/ingresar/contrasena/',
                      data=parametros, verify=settings.AUTANA_SSL_VERIFY)
    return json.loads(r.content)


def obtener_estados():
    """
    Función que permite obtener la lista de estados
    El método hace un llamado a un servicio REST de la aplicación comun
    @return: Lista de estados
    """

    r = requests.get(settings.APP_COMUN_URL + 'ubicacion/estados',
                     verify=settings.AUTANA_SSL_VERIFY)
    estados = json.loads(r.content)

    return estados


def obtener_municipios(id_estado):
    """
    Función que permite obtener la lista de municipios asociados a un estado

    El método hace un llamado a un servicio REST de la aplicación comun

    @param id_estado: Identificador del estado
    @type id_estado: entero

    @return: Lista de municipios asociados al estado
    """

    r = requests.get(settings.APP_COMUN_URL + 'ubicacion/municipios/' + str(id_estado),
                     verify=settings.AUTANA_SSL_VERIFY)
    municipios = json.loads(r.content)

    return municipios


def obtener_municipios_json(id_estado):
    """
    Función que permite obtener la lista de municipios asociados a un estado

    El método hace un llamado a un servicio REST de la aplicación comun

    @param id_estado: Identificador del estado
    @type id_estado: entero

    @return: Lista de municipios asociados al estado en formato JSON
    """

    r = requests.get(settings.APP_COMUN_URL + 'ubicacion/municipios/' + str(id_estado),
                     verify=settings.AUTANA_SSL_VERIFY)

    return r.content


def obtener_parroquias(id_municipio):
    """
    Función que permite obtener la lista de parroquias asociadas a un municipio

    El método hace un llamado a un servicio REST de la aplicación comun

    @param id_municipio: Identificador del municipio
    @type id_municipio: entero

    @return: Lista de parroquias asociadas al municipio
    """

    r = requests.get(settings.APP_COMUN_URL + 'ubicacion/parroquias/' + str(id_municipio),
                     verify=settings.AUTANA_SSL_VERIFY)
    parroquias = json.loads(r.content)

    return parroquias


def obtener_parroquias_json(id_municipio):
    """
    Función que permite obtener la lista de parroquias asociadas a un municipio

    El método hace un llamado a un servicio REST de la aplicación comun

    @param id_municipio: Identificador del municipio
    @type id_municipio: entero

    @return: Lista de parroquias asociadas al municipio en formato JSON
    """

    r = requests.get(settings.APP_COMUN_URL + 'ubicacion/parroquias/' + str(id_municipio),
                     verify=settings.AUTANA_SSL_VERIFY)

    return r.content


def obtener_municipio_por_parroquia(id_parroquia):
    """
    Función que permite obtener el municipio asociado a una parroquia

    El método hace un llamado a un servicio REST de la aplicación comun

    @param id_parroquia: Identificador de la parroquia
    @type id_parroquia: entero

    @return: El municipio asociado a la parroquia
    """

    r = requests.get(settings.APP_COMUN_URL + 'ubicacion/municipio_por_parroquia/' + str(id_parroquia),
                     verify=settings.AUTANA_SSL_VERIFY)
    municipio = json.loads(r.content)

    return municipio


def obtener_estado_por_municipio(id_municipio):
    """
    Función que permite obtener el estado asociado a un municipio

    El método hace un llamado a un servicio REST de la aplicación comun

    @param id_municipio: Identificador del municipio
    @type id_municipio: entero

    @return: El estado asociado al municipio
    """

    r = requests.get(settings.APP_COMUN_URL + 'ubicacion/estado_por_municipio/' + str(id_municipio),
                     verify=settings.AUTANA_SSL_VERIFY)
    estado = json.loads(r.content)

    return estado


def obtener_parroquia(id_parroquia):
    """
    Función que permite obtener la parroquia asociada al identificador

    El método hace un llamado a un servicio REST de la aplicación comun

    @param id_parroquia: Identificador de la parroquia
    @type id_parroquia: entero

    @return: La parroquia asociado al identificador
    """

    r = requests.get(settings.APP_COMUN_URL + 'ubicacion/parroquia/' + str(id_parroquia),
                     verify=settings.AUTANA_SSL_VERIFY)
    parroquia = json.loads(r.content)

    return parroquia
