# -*- coding: utf-8 -*-


from captcha.conf import settings as settings_captcha
from captcha.models import CaptchaStore
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils.http import base36_to_int

from comun.aplicacion_comun import validar_usuario_token, modificar_contrasena, \
    obtener_municipios_json, obtener_parroquias_json
from comun.forms import FormularioModificarContrasena
from proyectos_app.models.usuarioSolicitante import UsuarioSolicitante


def refrescar_captcha(request):
    """
    Crea una nueva imagen para el captcha y retorna la clave asociada

    @return: La clave asociada a la nueva imagen
    """

    challenge, response = settings_captcha.get_challenge()()
    store = CaptchaStore.objects.create(challenge=challenge, response=response)
    key = store.hashkey
    return HttpResponse(key)


def confirmar(request):
    """
    Muestra el formulario para el cambio de contraseña validando el token
    obtenido por URL

    @return: El formulario para el cambio de contraseña
    """

    form = FormularioModificarContrasena()

    uidb36 = request.GET.get('uid', '')
    token = request.GET.get('token', '')

    if uidb36 is not None and token is not None:

        usuario = validar_usuario_token(uidb36, token)

        if('error' in usuario):
            return render_to_response('comun/restablecer_contrasena.html',
                                      { 'form': form, 'mensaje_error': usuario['error']},
                                      context_instance=RequestContext(request))
        else:
            uid_int = base36_to_int(uidb36)
            request.session['uid'] = uid_int
            return render_to_response('comun/restablecer_contrasena.html',
                                      { 'form': form },
                                      context_instance=RequestContext(request))

    else:
        return render_to_response('comun/restablecer_contrasena.html',
                                  { 'form': form, 'mensaje_error': 'El link no es válido' },
                                  context_instance=RequestContext(request))


def restablecer_contrasena(request):
    """
    Permite restablecer la contraseña del usuario retornando un formulario
    con el mensaje correspondiente

    @return: El formulario con el mensaje de la operación realizada
    """

    if request.method == 'POST':

        form = FormularioModificarContrasena(request.POST)
        if form.is_valid() and 'uid' in request.session:
            contrasena = form.cleaned_data['contrasena']
            usuario = modificar_contrasena(request.session['uid'], contrasena)
            if('error' in usuario):
                return render_to_response('comun/restablecer_contrasena.html',
                        { 'form': form, 'mensaje_error': usuario['error']},
                        context_instance=RequestContext(request))
            else:

                try:
                    usuario_solicitante = UsuarioSolicitante.objects.get(usuario=usuario['id'])
                except UsuarioSolicitante.DoesNotExist:
                    usuario_solicitante = None

                if usuario_solicitante:
                    request.session['usuario_solicitante_proyectos'] = usuario_solicitante
                    request.session['permisos'] = usuario['permisos']
                    registrar_sesion(request, usuario_solicitante.id)
                    request.session.set_expiry(settings.SESSION_EXPIRY)
                    return render_to_response('proyectos/inicio.html', { },
                            context_instance=RequestContext(request))
        else:
            return render_to_response('comun/restablecer_contrasena.html', { 'form': form },
                        context_instance=RequestContext(request))


def registrar_sesion(request, id_usuario):
    """
    Función que permite registrar la operadora en sesión

    @param id_usuario: Identificador del usuario de la operadora
    @type id_usuario: entero
    """

    try:
        request.session['solicitante'] = UsuarioSolicitante.objects.get(pk=id_usuario)
    except UsuarioSolicitante.DoesNotExist:
        request.session['solicitante'] = None


def municipios(request):
    """
    Permite obtener la lista de municipios asociados a un estado

    @param id_estado: Identificador del estado

    @return: La lista de municipios
    """

    id_estado = request.GET.get('id_estado')
    municipios = obtener_municipios_json(id_estado)
    return HttpResponse(municipios, mimetype="application/javascript")


def parroquias(request):
    """
    Permite obtener la lista de parroquias asociadas a un municipio

    @param id_municipio: Identificador del municipio

    @return: La lista de parroquias
    """

    id_municipio = request.GET.get('id_municipio')
    parroquias = obtener_parroquias_json(id_municipio)
    return HttpResponse(parroquias, mimetype="application/javascript")
