# -*- coding: utf-8 -*-

# Código de permisos para usuarios de proyectos
PERMISOS_PROYECTOS = {
    'MODIFICAR_USUARIO_SOLICITANTE': 'proyectos_usuario_solicitante.change_usuario_solicitante',
    'ELIMINAR_USUARIO_SOLICITANTE': 'proyectos_usuario_solicitante.delete_usuario_solicitante',
    'VER_USUARIO_SOLICITANTE': 'proyectos_usuario_solicitante.view_usuario_solicitante',
    'CREAR_FICHA_USUARIO': 'create_ficha_usuario',
    'FICHA_NATURAL': 'ficha_natural',
    'FICHA_JURIDICA': 'ficha_juridica',
}


# Tipo persona
TIPO_PERSONA_OPCIONES = (
    ('V', 'V'),
    ('E', 'E'),
    ('G', 'G'),
    ('J', 'J'),
    ('P', 'P'),
)

# Tipo cédula
TIPO_CEDULA_OPCIONES = (
    ('V', 'V'),
    ('E', 'E'),
)

# Sexo
SEXO_OPCIONES = (
    ('M', 'Masculino'),
    ('F', 'Femenino'),
)

# Estado Civil
ESTADO_CIVIL_OPCIONES = (
    ('S', 'Soltero(a)'),
    ('C', 'Casado(a)'),
    ('V', 'Viudo(a)'),
    ('D', 'Divorciado(a)'),
    ('O', 'Concubino(a)'),
    ('E', 'Separado(a) de Cuerpos'),
)

# Area, define el origen del capital de funcionamiento de una empresa
AREA_OPCIONES = {
    ('Público', 'PUBLICO'),
    ('Privado', 'PRIVADO'),
    ('Mixto', 'MIXTO'),
    ('Particular', 'PARTICULAR'),
}

# Define los tipos de usuario que pueden existir en la aplicacion
USUARIO_OPCIONES = {
    ('Natural', 'NATURAL'),
    ('Juridico', 'JURIDICO'),
}

CAMBIAR_CONTRASENA_SUBJECT = 'Cambio de Contraseña'
INGRESAR_CONTRASENA_SUBJECT = 'Ingreso de Contraseña'

APP_NAME = 'PROYECTOS'
