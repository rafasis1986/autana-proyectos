# -*- coding: utf-8 -*-

from django import template


register = template.Library()

@register.inclusion_tag('tipo_persona.html')
def tipo_persona(*args, **kwargs):
    """
    Permite obtener un tag con las opciones de tipo de persona
    """

    tipo_id = kwargs['id']
    tipo_name = kwargs['name']
    lista_tipo_persona = ('E', 'G', 'J', 'P', 'V',)
    return {'lista_tipo_persona': lista_tipo_persona, 'tipo_id' : tipo_id, 'tipo_name': tipo_name}
