# -*- coding: utf-8 -*-
'''
Created on 07/05/2014

@author: rtorres
'''
from django import forms

from comun.aplicacion_comun import obtener_estados, obtener_municipio_por_parroquia, \
    obtener_estado_por_municipio, obtener_municipios, obtener_parroquias
from proyectos_app.models.abstracts import DireccionFiscal


class FormularioDireccionFiscal(forms.ModelForm):
    estados = obtener_estados()
    estado = forms.ChoiceField(required=True, choices=[("", "Seleccione Estado")] +
    [(est["id"], est["nombre"]) for est in estados])
    estado.widget.attrs.update({'style': 'width: 180px'})
    municipio = forms.ChoiceField(required=True,)
    municipio.widget.attrs.update({'style': 'width: 180px'})
    parroquia = forms.ChoiceField(required=True,)
    parroquia.widget.attrs.update({'style': 'width: 180px'})

    class Meta():
        model = DireccionFiscal
        fields = ('estado', 'municipio', 'parroquia',)

# ...................................................................................................
#     def __init__(self, id_parroquia, *args, **kwargs):
#         '''
#         Permite inicializar los atributos de los controles y establecer los municipios y parroquias
#
#         @param id_parroquia: el identificador de la parroquia asociada a la operadora.
#         @type  id_parroquia: entero
#         '''
#         # super(DireccionFiscal, self).__init__(*args, **kwargs)
#         if id_parroquia:
#             municipio = obtener_municipio_por_parroquia(id_parroquia)
#             estado = obtener_estado_por_municipio(municipio["id"])
#             municipios = obtener_municipios(estado["id"])
#             parroquias = obtener_parroquias(municipio["id"])
#             self.fields['municipio'].choices = [("", "Seleccione Municipio")] \
#                 + [(mun["id"], mun["nombre"]) for mun in municipios]
#             self.fields['parroquia'].choices = [("", "Seleccione Parroquia")] \
#                 + [(parr["id"], parr["nombre"]) for parr in parroquias]
#             self.fields['estado'].initial = estado["id"]
#             self.fields['municipio'].initial = municipio["id"]
#             self.fields['parroquia'].initial = id_parroquia
#         else:
#             self.fields['municipio'].choices = [("", "Seleccione Municipio")]
#             self.fields['parroquia'].choices = [("", "Seleccione Parroquia")]
#         return DireccionFiscal.__init__(self, *args, **kwargs)
# ...................................................................................................
