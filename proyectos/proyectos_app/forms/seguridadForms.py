# -*- coding: utf-8 -*-
'''
Created on 28/04/2014

@author: rtorres
'''
from captcha.fields import CaptchaField
from django.forms.fields import CharField, EmailField
from django.forms.models import ModelChoiceField

from comun.forms import FormularioModificarContrasena
from comun.mensajes import MENSAJES_VALIDACION
from proyectos_app.forms.utilsForms import FormularioRIFForm
from proyectos_app.models.abstracts import Seguridad
from proyectos_app.models.preguntaSeguridad import PreguntaSeguridad


class FormularioRegistroContrasena(FormularioModificarContrasena):
    respuesta_seguridad = CharField(required=True, max_length=255, min_length=1,
        error_messages={'required': 'Debe ingresar una respuesta a su pregunta de seguridad'})
    pregunta_seguridad = ModelChoiceField(required=True, queryset=PreguntaSeguridad.objects.all(),
        error_messages={'required': 'Debe seleccionar una pregunta de seguridad'})

    class Meta:
        model = Seguridad
        fields = ['contrasena', 'conf_contrasena', 'pregunta_seguridad', 'respuesta_seguridad']
        abstract = True


class FormularioRecuperaContrasena(FormularioRIFForm):
    respuesta_seguridad = CharField(required=True, max_length=255, min_length=1,
        error_messages={'required': 'Debe ingresar una respuesta a su pregunta de seguridad'})
    pregunta_seguridad = ModelChoiceField(required=True, queryset=PreguntaSeguridad.objects.all(),
        error_messages={'required': 'Debe seleccionar una pregunta de seguridad'})
    correo = EmailField(required=True, max_length=254,
        error_messages={'required': 'El Correo Electrónico es un campo obligatorio',
                'invalid': 'Ingrese un Correo Electrónico válido'})
    captcha = CaptchaField(label="captcha", error_messages={'invalid':
        MENSAJES_VALIDACION['CAPTCHA_NO_VALIDO']}, required=False)

    class Meta:
        model = Seguridad
        fields = ('tipo_per', 'rif', 'rif_fin', 'pregunta_seguridad', 'respuesta_seguridad',
                  'correo', 'captcha')
        abstract = True
