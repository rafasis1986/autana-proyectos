# -*- coding: utf-8 -*-
'''
Created on 08/05/2014

@author: rtorres
'''
from django import forms

from proyectos_app.models.abstracts import InfoEmpleado


class FormularioInfoEmpleado(forms.ModelForm):

    class Meta:
        model = InfoEmpleado
        fields = ('profesion', 'cargo', 'dependencia',)
