# -*- coding: utf-8 -*-
'''
Created on 26/04/2014

@author: rtorres
'''

from captcha.fields import CaptchaField
from django.forms.fields import CharField
from django.forms.models import ModelChoiceField
from django.forms.widgets import PasswordInput

from comun.forms import FormularioPersona
from comun.mensajes import MENSAJES_VALIDACION
from proyectos_app.forms.utilsForms import FormularioRIF
from proyectos_app.models.preguntaSeguridad import PreguntaSeguridad
from proyectos_app.models.usuarioSolicitante import UsuarioSolicitante


class FormularioRegistroSolicitante(FormularioPersona, FormularioRIF):
    captcha = CaptchaField(label="captcha", error_messages={'invalid':
        MENSAJES_VALIDACION['CAPTCHA_NO_VALIDO']}, required=False)
    respuesta_seguridad = CharField(required=True, max_length=255, min_length=1,
        error_messages={'required': 'Debe ingresar una respuesta a su pregunta de seguridad'})
    pregunta_seguridad = ModelChoiceField(required=True, queryset=PreguntaSeguridad.objects.all(),
        error_messages={'required': 'Debe seleccionar una pregunta de seguridad'})

    class Meta:
        model = UsuarioSolicitante
        fields = ['nombre', 'apellido', 'correo', 'pregunta_seguridad', 'respuesta_seguridad',
                  'captcha']


class FormularioLoginSolicitante(FormularioRIF):
    contrasena = CharField(widget=PasswordInput(attrs={'style': 'width: 90px',
        'class': 'flotadoi'}), required=True)
#     captcha = CaptchaField(label="", error_messages={
#             'invalid': MENSAJES_VALIDACION['CAPTCHA_NO_VALIDO']})

    class Meta:
        model = UsuarioSolicitante
        fields = ('tipo_per', 'rif', 'rif_fin', 'contrasena',)
