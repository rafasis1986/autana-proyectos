# -*- coding: utf-8 -*-
'''
Created on 07/05/2014

@author: rtorres
'''

from proyectos_app.forms.utilsForms import FormularioTelefono
from proyectos_app.models.abstracts import InformacionContacto
from django import forms


class FormularioInformacionContacto(FormularioTelefono):
    correo = forms.EmailField()

    class Meta():
        model = InformacionContacto
        fields = ('telefono', 'correo', 'direccion',)
