# -*- coding: utf-8 -*-
'''
Created on 27/04/2014

@author: rtorres
'''

from comun.constantes import TIPO_PERSONA_OPCIONES
from comun.forms import VEPhoneNumberField
from comun.mensajes import MENSAJES_VALIDACION
from comun.verificarRIF import verificarRif
import floppyforms as forms


class FormularioRIF(forms.ModelForm):
    """
    Clase que contiene el formulario que permite ingresar el rif
    """

    tipo_per = forms.ChoiceField(required=True, choices=TIPO_PERSONA_OPCIONES)
    tipo_per.widget.attrs.update({'class': 'tiny'})

    rif = forms.CharField(required=True, max_length=8, min_length=8)
    rif.widget.attrs.update({'style': 'width: 90px'})

    rif_fin = forms.CharField(required=True, max_length=1)
    rif_fin.widget.attrs.update({'style': 'width: 15px'})

    def clean_rif(self):
        """
        Método que valida que el RIF sea numérico
        @return: Un mensaje de error si el RIF no es numérico
        """
        rif = self.cleaned_data.get('rif')
        if not rif.isdigit():
            raise forms.ValidationError(MENSAJES_VALIDACION['RIF_NO_NUMERICO'])
        return rif

    def clean_rif_fin(self):
        """
        Método que valida que el último dígito del RIF sea numérico y que el
        RIF sea válido

        @return: Un mensaje de error si el último dígito del RIF no es numérico
        """
        rif_fin = self.cleaned_data.get('rif_fin')
        if not rif_fin.isdigit():
            raise forms.ValidationError(MENSAJES_VALIDACION['RIF_NO_NUMERICO'])

        tipo_per = self.cleaned_data.get('tipo_per')
        rif = self.cleaned_data.get('rif')
        if not verificarRif(tipo_per + rif + rif_fin):
            raise forms.ValidationError(MENSAJES_VALIDACION['RIF_NO_VALIDO'])

        return rif_fin


class FormularioRIFForm(forms.Form):
    """
    Clase que contiene el formulario que permite ingresar el rif
    """

    tipo_per = forms.ChoiceField(required=True, choices=TIPO_PERSONA_OPCIONES)
    tipo_per.widget.attrs.update({'class': 'tiny'})

    rif = forms.CharField(required=True, max_length=8, min_length=8)
    rif.widget.attrs.update({'style': 'width: 90px'})

    rif_fin = forms.CharField(required=True, max_length=1)
    rif_fin.widget.attrs.update({'style': 'width: 15px'})

    def clean_rif(self):
        """
        Método que valida que el RIF sea numérico
        @return: Un mensaje de error si el RIF no es numérico
        """
        rif = self.cleaned_data.get('rif')
        if not rif.isdigit():
            raise forms.ValidationError(MENSAJES_VALIDACION['RIF_NO_NUMERICO'])
        return rif

    def clean_rif_fin(self):
        """
        Método que valida que el último dígito del RIF sea numérico y que el
        RIF sea válido

        @return: Un mensaje de error si el último dígito del RIF no es numérico
        """
        rif_fin = self.cleaned_data.get('rif_fin')
        if not rif_fin.isdigit():
            raise forms.ValidationError(MENSAJES_VALIDACION['RIF_NO_NUMERICO'])

        tipo_per = self.cleaned_data.get('tipo_per')
        rif = self.cleaned_data.get('rif')
        if not verificarRif(tipo_per + rif + rif_fin):
            raise forms.ValidationError(MENSAJES_VALIDACION['RIF_NO_VALIDO'])

        return rif_fin


class FormularioTelefono(forms.ModelForm):
    """
    Clase que contiene el formulario que permite ingresar y modificar
    teléfonos
    """

    telefono = VEPhoneNumberField()

    class Meta:
        abstract = True
