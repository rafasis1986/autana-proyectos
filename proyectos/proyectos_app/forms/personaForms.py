# -*- coding: utf-8 -*-
'''
Created on 06/05/2014

@author: rtorres
'''
from comun.forms import FormularioPersona
from proyectos_app.forms.direccionFiscalForms import FormularioDireccionFiscal
from proyectos_app.forms.empleadoForms import FormularioInfoEmpleado
from proyectos_app.forms.informacionContactoForms import FormularioInformacionContacto
from proyectos_app.forms.utilsForms import FormularioRIF
from proyectos_app.models.persona import PersonaJuridica, PersonaNatural, FichaUsuario
from django import forms


class FormularioPersonaJuridica(FormularioInformacionContacto, FormularioDireccionFiscal,
        FormularioRIF):

    razon_social = forms.CharField(label='nombre')

    class Meta:
        model = PersonaJuridica

    def __init__(self, *args, **kwargs):
        super(forms.ModelForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = [
            'razon_social',
            'direccion',
            'telefono',
            'tipo_per',
            'rif',
            'rif_fin',
            'zona_postal',
            'correo',
            'area',
            'estado',
            'municipio',
            'parroquia'
            ]


class FormularioPersonaNatural(FormularioInformacionContacto, FormularioPersona, FormularioRIF):

    class Meta:
        model = PersonaNatural

    def __init__(self, *args, **kwargs):
        super(forms.ModelForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = [
            'nombre',
            'apellido',
            'direccion',
            'tipo_per',
            'rif',
            'telefono',
            'fecha_nacimiento',
            'sexo',
            'correo',
            'estado_civil'
            ]


class FormularioFichaUsuario(FormularioInfoEmpleado):

    class Meta:
        model = FichaUsuario

    def __init__(self, *args, **kwargs):
        super(forms.ModelForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = [
            'profesion',
            'cargo',
            'dependencia'
            ]
