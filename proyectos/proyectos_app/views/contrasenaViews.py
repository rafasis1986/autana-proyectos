# -*- coding: utf-8 -*-
'''
Created on 27/04/2014

@author: rtorres
'''
from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.template.context import Context, RequestContext
from django.template.loader import get_template
from django.utils.http import base36_to_int, int_to_base36
from django.views.generic.edit import FormView

from comun.aplicacion_comun import usuario_ingresar_contrasena_grupo, validar_usuario_token, \
    restablecer_contrasena
from comun.constantes import CAMBIAR_CONTRASENA_SUBJECT
from comun.mensajes import MENSAJES_SOLICITANTE
from proyectos_app.constantes import GRUPO_PROYECTOS
from proyectos_app.forms.seguridadForms import FormularioRegistroContrasena, \
    FormularioRecuperaContrasena
from proyectos_app.models.abstracts import Seguridad
from proyectos_app.models.usuarioSolicitante import UsuarioSolicitante
from proyectos_app.views.utilsView import validar_pregunta_seguridad
import smtplib


TEMP_FORM = 'proyectos/ingresar_contrasena.html'
TEMP_FORM_RECOVERY = 'proyectos/recuperar_contrasena.html'


class ContrasenaMixin(object):
    model = Seguridad


class ContrasenaFormMixin():
    form_class = FormularioRegistroContrasena
    template_name = TEMP_FORM


class NewContrasena(ContrasenaFormMixin, FormView):

    def get_context_data(self, **kwargs):
        if self.request.GET.get('uid') and self.request.GET.get('token'):
            uidb36 = self.request.GET.get('uid')
            token = self.request.GET.get('token')
            self.request.session['uid'] = uidb36
            self.request.session['token'] = token
        return FormView.get_context_data(self, **kwargs)

    def form_valid(self, form):
        retorno = None
        uidb36 = self.request.session['uid']
        token = self.request.session['token']
        if uidb36 is not None and token is not None:
            usuario = validar_usuario_token(uidb36, token)
            if('error' in usuario):
                messages.error(self.request, usuario['error'])
                retorno = FormView.form_invalid(self, form)
            else:
                uid_int = base36_to_int(uidb36)
                self.request.session['uid'] = uid_int
                contrasena = form.cleaned_data['contrasena']
                pregunta = form.cleaned_data['pregunta_seguridad']
                respuesta = form.cleaned_data['respuesta_seguridad']
                respuesta = respuesta.strip().upper()
                try:
                    solicitante = UsuarioSolicitante.objects.get(usuario=uid_int)
                    valida_pregunta = validar_pregunta_seguridad(solicitante, pregunta, respuesta)
                    if valida_pregunta is not None:
                        messages.error(self.request, valida_pregunta)
                        retorno = FormView.form_invalid(self, form)
                    else:
                        usuario = usuario_ingresar_contrasena_grupo(
                            self.request.session['uid'], contrasena, GRUPO_PROYECTOS)
                        if('error' in usuario):
                            messages.error(self.request, usuario['error'])
                            retorno = FormView.form_invalid(self, form)
                        else:
                            self.request.session['usuario_solicitante'] = solicitante
                            self.request.session['permisos'] = usuario['permisos']
                            messages.success(self.request, MENSAJES_SOLICITANTE['CAMBIO_CONTRASENA_EXITO'])
                            retorno = render_to_response('proyectos/dashboard.html', {},
                                        context_instance=RequestContext(self.request))
                except:
                    messages.error(self.request, MENSAJES_SOLICITANTE['LOGIN_USUARIO_NO_VALIDO'])
                    retorno = FormView.form_invalid(self, form)
        else:
            messages.error(self.request, 'Enlace invalido')
            retorno = FormView.form_invalid(self, form)
        return retorno


class RecoveryContrasena(FormView):
    template_name = TEMP_FORM_RECOVERY
    form_class = FormularioRecuperaContrasena

    def form_valid(self, form):
        retorno = None
        correo = form.cleaned_data['correo']
        pregunta = form.cleaned_data['pregunta_seguridad']
        respuesta = form.cleaned_data['respuesta_seguridad']
        respuesta = respuesta.strip().upper()
        rif = form.cleaned_data['rif']
        rif_fin = form.cleaned_data['rif_fin']
        tipo_per = form.cleaned_data['tipo_per']
        try:
            solicitante = UsuarioSolicitante.objects.get(tipo_cedula=tipo_per, cedula=rif)
            valida_pregunta = validar_pregunta_seguridad(solicitante, pregunta, respuesta)
            if valida_pregunta is not None:
                messages.error(self.request, valida_pregunta)
                retorno = FormView.form_invalid(self, form)
            else:
                nombre_usuario = tipo_per + '-' + rif + '-' + rif_fin
                usuario = restablecer_contrasena(nombre_usuario, correo)
                if('error' in usuario):
                    messages.error(self.request, usuario['error'])
                    retorno = FormView.form_invalid(self, form)
                else:
                    try:
                        link = self.request.build_absolute_uri(
                        "%s?uid=%s&token=%s" % (reverse('ingresar_contrasena'), int_to_base36(usuario['id']),
                            usuario['token']))
                        t = get_template("comun/reiniciar_contrasena.mail")
                        c = Context({'link': link})
                        send_mail(CAMBIAR_CONTRASENA_SUBJECT, t.render(c), settings.EMAIL_FROM, [correo],
                                  fail_silently=False)
                        retorno = FormView.form_valid(self, form)
                    except smtplib.SMTPException:
                        messages.error(self.request, MENSAJES_SOLICITANTE['ERROR_LINK_CONTRASENA'])
                        retorno = FormView.form_invalid(self, form)
        except:
            messages.error(self.request, MENSAJES_SOLICITANTE['LOGIN_USUARIO_NO_VALIDO'])
            retorno = FormView.form_invalid(self, form)
        return retorno

    def get_success_url(self):
        messages.success(self.request, MENSAJES_SOLICITANTE['CORREO_REGISTRO_EXITO'])
        return reverse('login')
