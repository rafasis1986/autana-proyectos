#!/usr/bin/env python
# -*- coding: utf-8 -*-


from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils.http import base36_to_int

from comun.aplicacion_comun import validar_usuario_token, usuario_ingresar_contrasena_grupo
from comun.forms import FormularioModificarContrasena
from proyectos import settings
from proyectos_app.models.usuarioSolicitante import UsuarioSolicitante


def ingresar_contrasena(request):
    """
    Muestra el formulario para el ingreso de contraseña validando el token 
    obtenido por URL

    @return: El formulario para el ingreso de contraseña
    """

    if request.method == 'POST':
        form = FormularioModificarContrasena(request.POST)
        if form.is_valid() and 'uid' in request.session:
            contrasena = form.cleaned_data['contrasena']
            usuario = usuario_ingresar_contrasena_grupo(request.session['uid'], contrasena
                    , 'GRUPO_PROYECTOS')

            if('error' in usuario):
                return render_to_response('proyectos/ingresar_contrasena.html',
                                          { 'form': form, 'mensaje_error': usuario['error']},
                                          context_instance=RequestContext(request))
            else:
                try:
                    usuario_solicitante = UsuarioSolicitante.objects.get(id_usuario=usuario['id'])
                except UsuarioSolicitante.DoesNotExist:
                    usuario_recaudacion = None
                if usuario_recaudacion:
                    request.session['usuario_solicitante'] = usuario_solicitante
                    request.session['permisos'] = usuario['permisos']
#                     registrar_sesion_operadora(request, usuario_recaudacion.id)
                    request.session.set_expiry(settings.SESSION_EXPIRY)
                    return render_to_response('operadora/inicio.html',
                                              { 'mensaje_exito': 'exito 1'},
                                              context_instance=RequestContext(request))
        else:
            return render_to_response('proyectos/ingresar_contrasena.html',
                                      { 'form': form },
                                      context_instance=RequestContext(request))

    else:
        form = FormularioModificarContrasena()
        uidb36 = request.GET.get('uid', '')
        token = request.GET.get('token', '')
        if uidb36 is not None and token is not None:
            usuario = validar_usuario_token(uidb36, token)
            if('error' in usuario):
                return render_to_response('proyectos/ingresar_contrasena.html',
                                          { 'form': form, 'mensaje_error': usuario['error']},
                                          context_instance=RequestContext(request))
            else:
                uid_int = base36_to_int(uidb36)
                request.session['uid'] = uid_int
                return render_to_response('proyectos/ingresar_contrasena.html',
                                          { 'form': form },
                                          context_instance=RequestContext(request))
        else:
            return render_to_response('proyectos/ingresar_contrasena.html',
                                      { 'form': form, 'mensaje_error': 'LINK_NO_VALIDO'},
                                      context_instance=RequestContext(request))
