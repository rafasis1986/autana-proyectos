# -*- coding: utf-8 -*-
'''
Created on 02/05/2014

@author: rtores
'''
from django.contrib.auth import logout
from django.core.urlresolvers import reverse
from django.views.generic.base import TemplateView, RedirectView

from proyectos_app.views.utilsView import LoginRequeridoMixin, inicializar_session


class DashboardView(LoginRequeridoMixin, TemplateView):
    template_name = 'proyectos/dashboard.html'

    def get_context_data(self, **kwargs):
        inicializar_session(self)
        return TemplateView.get_context_data(self, **kwargs)


class LogOutView(RedirectView):

    permanent = False
    query_string = True

    def get_redirect_url(self):
        logout(self.request)
        return reverse('login')
