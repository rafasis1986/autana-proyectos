# -*- coding: utf-8 -*-
'''
Created on 11/05/2014

@author: rtorres
'''
from django.views.generic.edit import CreateView

from proyectos_app.forms.personaForms import FormularioPersonaNatural
from proyectos_app.models.persona import PersonaNatural
from proyectos_app.views.utilsView import LoginRequeridoMixin


class PersonaNaturalMixin(LoginRequeridoMixin, object):
    model = PersonaNatural


class PersonaNaturalFormMixin(PersonaNaturalMixin):
    form_class = FormularioPersonaNatural
    template_name = 'proyectos/object_form.html'


class NewPersonaNaturalView(PersonaNaturalFormMixin, CreateView):
    pass
