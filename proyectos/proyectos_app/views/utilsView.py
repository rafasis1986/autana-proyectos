# -*- coding: utf-8 -*-
'''
Created on 03/05/2014

@author: rtores
'''
from braces.views._access import AccessMixin
from django.contrib import messages
from django.contrib.auth.views import redirect_to_login
from django.core.exceptions import PermissionDenied

from comun.mensajes import MENSAJES_SOLICITANTE
from proyectos_app.constantes import usuario_solicitante, MSG_LOGIN_REQUERIDO
from proyectos_app.models.persona import FichaUsuario


def validar_pregunta_seguridad(solicitante, pregunta, respuesta):
    '''
    funcion que verifica si la pregunta y respuesta ingresada por el usuario, corresponden a las que
    se hallan almacenadas en la base de datos de la aplicacin

    @requires: solicitane    Un objeto de la clase UsuarioSolicitante
                pregunta    Un obteto de la clase PreguntaSecreta
                respuesta    Un String con la respuest ingresada por el usuario

    @return: None    Si la pregunta ha sido verificada exitosamente
            String   Con el error ocurrido a la hora de la verificacion
    '''
    retorno = None
    if solicitante is None or pregunta is None or respuesta is None:
        retorno = MENSAJES_SOLICITANTE['PREGUNTA_SEGURIDAD_ERROR']
    elif solicitante.pregunta_seguridad != pregunta:
        retorno = MENSAJES_SOLICITANTE['PREGUNTA_NO_VALIDA']
    elif solicitante.respuesta_seguridad != respuesta:
        retorno = MENSAJES_SOLICITANTE['RESPUESTA_NO_VALIDA']
    return retorno


class LoginRequeridoMixin(AccessMixin):
    '''
    Se creaa un wraper de la clase LoginRequiredMixin, cambiandole la condicion de validacion por
    una creada para la plicacion, recordando que en la misma nuestros usuarios no extienden del
    User de django
    '''
    def dispatch(self, request, *args, **kwargs):
        respuesta = redirect_to_login(request.get_full_path(), self.get_login_url(),
            self.get_redirect_field_name())
        try:
            if self.request.session[usuario_solicitante]:
                respuesta = super(LoginRequeridoMixin, self).dispatch(request, *args, **kwargs)
            elif self.raise_exception:
                raise PermissionDenied  # return a forbidden response
        except KeyError:
            messages.warning(self.request, MSG_LOGIN_REQUERIDO)
        return respuesta


def inicializar_session(vista):
    '''
    funcion que permite inicializar las variables de session cuando se loguea un usuario o cuando
    se redirige al dashboard

    @requires: vista Objeto de tipo View
    '''
    try:
        solicitante = vista.request.session[usuario_solicitante]
        ficha = None
        if solicitante is not None:
            try:
                ficha = FichaUsuario.objects.get(usuario=solicitante)
                vista.request.session['ficha'] = ficha.tipo
            except FichaUsuario.DoesNotExist:
                vista.request.session['ficha'] = 'create_ficha_usuario'
    except:
        pass


def eliminar_session(vista):
    '''
    funcion que elimina las variables de session cuando un usuario sale del sistema

    @requires: vista Objeto de tipo View
    '''
    pass
