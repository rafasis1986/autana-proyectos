# -*- coding: utf-8 -*-
'''
Created on 14/05/2014

@author: rtorres
'''
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.utils.datastructures import MultiValueDictKeyError
from django.views.generic.edit import CreateView

from comun.constantes import PERMISOS_PROYECTOS
from proyectos_app.constantes import TIPO_USUARIO, usuario_solicitante
from proyectos_app.forms.personaForms import FormularioFichaUsuario, FormularioPersonaNatural, \
    FormularioPersonaJuridica
from proyectos_app.models.persona import FichaUsuario
from proyectos_app.views.utilsView import LoginRequeridoMixin
from django.db.transaction import commit_on_success


class FichaUsuarioMixin(LoginRequeridoMixin, object):
    model = FichaUsuario


class FichaUsuarioFormMixin(FichaUsuarioMixin):
    form_class = FormularioFichaUsuario
    template_name = 'proyectos/object_form.html'


class NewFichaUsuarioView(FichaUsuarioFormMixin, CreateView):
    persona_form = FormularioPersonaNatural
    empresa_form = FormularioPersonaJuridica
    tipo_ficha = None

    def get(self, request, *args, **kwargs):
        super(NewFichaUsuarioView, self).get(request, *args, **kwargs)
        self.object = None
        form = self.form_class
        persona_form = self.persona_form
        empresa_form = self.empresa_form
        respuesta = self.render_to_response(self.get_context_data(object=self.object,
            form=form, persona_form=persona_form, empresa_form=empresa_form))
        return respuesta

    def get_context_data(self, **kwargs):
        context = CreateView.get_context_data(self, **kwargs)
        if 'persona_form' not in context:
            context['persona_form'] = self.persona_form(self.request.GET)
        if 'empresa_form' not in context:
            context['empresa_form'] = self.empresa_form(self.request.GET)
        return context

    def form_invalid(self, form, persona_form, empresa_form):
        return self.render_to_response(self.get_context_data(form=form, persona_form=persona_form,
                    empresa_form=empresa_form))

    @commit_on_success
    def form_valid(self, form, persona_form, empresa_form):
        self.object = form.save(commit=False)
        persona = persona_form.save(commit=False)
        empresa = empresa_form.save(commit=False)
        respuesta = None
        try:
            tipo_ficha = self.request.GET[TIPO_USUARIO]
            if tipo_ficha == PERMISOS_PROYECTOS['FICHA_NATURAL']:
                self.object = FichaUsuario()
                self.object.tipo = tipo_ficha
                empresa.save()
                persona.save()
                self.object.empresa = empresa
                self.object.persona = persona
                self.object.usuario = self.request.session[usuario_solicitante]
                self.object.save()
            elif tipo_ficha == PERMISOS_PROYECTOS['FICHA_JURIDICA']:
                pass
            else:
                messages.error(self.request, 'Error direccion no reconocida, accede através del menu')
                respuesta = self.form_invalid(form, persona_form, empresa_form)
            if respuesta is None:
                respuesta = HttpResponseRedirect(self.get_success_url())
        except MultiValueDictKeyError:
            messages.error(self.request, 'Error no hay GET')
            respuesta = self.form_invalid(form, persona_form, empresa_form)
        return respuesta

    def post(self, request, *args, **kwargs):
        respuesta = None
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        persona_form = FormularioPersonaNatural(self.request.POST)
        empresa_form = FormularioPersonaJuridica(self.request.POST)
        if (form.is_valid() and persona_form.is_valid() and empresa_form.is_valid()):
            respuesta = self.form_valid(form, persona_form, empresa_form)
        else:
            respuesta = self.form_invalid(form, persona_form, empresa_form)
        return respuesta
