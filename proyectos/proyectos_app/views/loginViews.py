# -*- coding: utf-8 -*-
'''
Created on 28/04/2014

@author: rtorres
'''

from django.conf import settings
from django.contrib import messages
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.views.generic.edit import FormView

from comun.aplicacion_comun import validar_usuario
from comun.mensajes import MENSAJES_SOLICITANTE
from proyectos_app.constantes import usuario_solicitante
from proyectos_app.forms.solicitanteForms import FormularioLoginSolicitante
from proyectos_app.models.usuarioSolicitante import UsuarioSolicitante
from proyectos_app.views.utilsView import inicializar_session


TEMP_FORM = 'proyectos/login.html'
TEMP_DASH = 'proyectos/dashboard.html'


class LoginView(FormView):
    '''
    Muestra el formulario de ingreso a la aplicación para los solicitantes de proyectos
    @return: El formulario de ingreso a la aplicación para los proyectos
    '''
    form_class = FormularioLoginSolicitante
    template_name = TEMP_FORM
    model = UsuarioSolicitante

    def form_valid(self, form):
        respuesta = None
        nombre_usuario = form.cleaned_data['tipo_per'] + "-" + form.cleaned_data['rif'] + "-" + \
            form.cleaned_data['rif_fin']
        contrasena = form.cleaned_data['contrasena']
        usuario = validar_usuario(nombre_usuario, contrasena)
        if('error' in usuario):
            messages.error(self.request, usuario['error'])
            respuesta = FormView.form_invalid(self, form)
        else:
            try:
                solicitante = UsuarioSolicitante.objects.get(usuario=usuario['id'])
            except UsuarioSolicitante.DoesNotExist:
                messages.error(self.request, MENSAJES_SOLICITANTE["LOGIN_USUARIO_NO_VALIDO"])
                respuesta = FormView.form_invalid(self, form)
            if respuesta is None:
                self.request.session[usuario_solicitante] = solicitante
                self.request.session['permisos'] = usuario['permisos']
                self.request.session.set_expiry(settings.SESSION_EXPIRY)
                inicializar_session(self)
                respuesta = render_to_response(TEMP_DASH, {},
                                context_instance=RequestContext(self.request))
        return respuesta
