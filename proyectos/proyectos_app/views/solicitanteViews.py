# -*- coding: utf-8 -*-
'''
Created on 26/04/2014

@author: rtorres
'''
from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db import transaction
from django.template.context import Context
from django.template.loader import get_template
from django.utils.http import int_to_base36
from django.views.generic.edit import CreateView

from comun.aplicacion_comun import nuevo_usuario_solicitante_proyectos
from comun.constantes import INGRESAR_CONTRASENA_SUBJECT
from comun.mensajes import MENSAJES_SOLICITANTE
from proyectos_app.forms.solicitanteForms import FormularioRegistroSolicitante
from proyectos_app.models.usuarioSolicitante import UsuarioSolicitante
import smtplib


OBJ_NAME = 'object_name'
OBJ_TEMP = 'object_template'
TEMP_FORM = 'proyectos/registro.html'


class SolicitanteMixin(object):
    model = UsuarioSolicitante

    def get_context_data(self, **kwargs):
        kwargs.update({OBJ_NAME: 'USUARIO'})
        kwargs.update({OBJ_TEMP: 'USUARIO_TEMP'})
        return kwargs


class SolicitanteFormMixin(SolicitanteMixin):
    form_class = FormularioRegistroSolicitante
    template_name = TEMP_FORM


class NewSolicitante(SolicitanteFormMixin, CreateView):

    @transaction.commit_on_success
    def form_valid(self, form):
        respuesta = None
        self.object = form.save(commit=False)
        nombre_usuario = form.cleaned_data['tipo_per'] + "-" + form.cleaned_data['rif'] + "-" + \
            form.cleaned_data['rif_fin']
        correo = form.cleaned_data['correo']
        usuario = nuevo_usuario_solicitante_proyectos(nombre_usuario, correo)
        if('error' in usuario):
            messages.error(self.request, usuario['error'])
            respuesta = CreateView.form_invalid(self, form)
        else:

            self.object.cedula = nombre_usuario[2:-2]
            self.object.tipo_cedula = nombre_usuario[0]
            self.object.usuario = usuario['id']
            self.object.save()
            try:
                link = self.request.build_absolute_uri(
                "%s?uid=%s&token=%s" % (reverse('ingresar_contrasena'), int_to_base36(usuario['id']),
                    usuario['token']))
                t = get_template("comun/ingresar_contrasena.mail")
                c = Context({'link': link})
                send_mail(INGRESAR_CONTRASENA_SUBJECT, t.render(c), settings.EMAIL_FROM, [correo],
                          fail_silently=False)
                respuesta = CreateView.form_valid(self, form)
            except smtplib.SMTPException:
                messages.warning(self.request, MENSAJES_SOLICITANTE['ERROR_LINK_CONTRASENA'])
                respuesta = CreateView.form_invalid(self, form)
        return respuesta

    def get_success_url(self):
        messages.success(self.request, MENSAJES_SOLICITANTE['CORREO_REGISTRO_EXITO'])
        return reverse('login')
