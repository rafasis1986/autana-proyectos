# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url

from proyectos_app.views.contrasenaViews import NewContrasena, RecoveryContrasena
from proyectos_app.views.dashboardViews import DashboardView, LogOutView
from proyectos_app.views.fichaUsuarioViews import NewFichaUsuarioView
from proyectos_app.views.loginViews import LoginView
from proyectos_app.views.solicitanteViews import NewSolicitante


# from proyectos_app.views.contrasenaViews import NewContrasena
# from operadora.views import ingresar_contrasena
app_urls = patterns('',
    url(r'^registrar/solicitante/$', NewSolicitante.as_view(), name='registrar_solicitante'),
    url(r'^ingresar/contrasena/$', NewContrasena.as_view(), name='ingresar_contrasena'),
    url(r'^recuperar/contrasena/$', RecoveryContrasena.as_view(), name='recuperar_contrasena'),
    url(r'^accounts/login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogOutView.as_view(), name='logout'),
    url(r'^$', DashboardView.as_view(), name='home'),
    url(r'^nueva/ficha/$', NewFichaUsuarioView.as_view(), name='crear_ficha'),
)

proyectos_app_paterns = app_urls
