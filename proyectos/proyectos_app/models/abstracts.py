# -*- coding: utf-8 -*-
'''
Created on 26/04/2014

@author: rtorres
'''
from django.db import models

from proyectos_app.models.miscelaneos import Profesion
from proyectos_app.models.preguntaSeguridad import PreguntaSeguridad


class Persona(models.Model):
    nombre = models.CharField(max_length=255)
    apellido = models.CharField(max_length=255)
    tipo_cedula = models.CharField(max_length=1)
    cedula = models.CharField(max_length=8)

    class Meta:
        unique_together = (("tipo_cedula", "cedula"),)
        abstract = True


class PersonaSolicitante(Persona):
    '''
    Clase base para los modelos que incluyen datos de personas asociadas a los proyectos
    '''
    correo = models.EmailField(max_length=255)

    class Meta(Persona.Meta):
        pass


class Recupera(models.Model):

    pregunta_seguridad = models.ForeignKey(PreguntaSeguridad)
    respuesta_seguridad = models.CharField('respuesta', max_length=255)

    class Meta:
        abstract = True


class Seguridad(Recupera):

    contrasena = models.CharField(max_length=255)
    conf_contrasena = models.CharField(max_length=255)
    nueva_contrasena = models.CharField(max_length=255)
    usuario = models.IntegerField()

    class Meta:
        abstract = True


class InformacionContacto(models.Model):

    telefono = models.CharField('teléfono', max_length=20)
    correo = models.EmailField(max_length=255)
    direccion = models.CharField('dirección', max_length=255)

    def __unicode__(self):
        return self.correo

    class Meta:
        abstract = True

    @property
    def dominio(self):
        return self.correo[self.correo.find('@'):]


class DireccionFiscal(models.Model):
    estado = models.CharField(max_length=32)
    municipio = models.CharField(max_length=32)
    parroquia = models.CharField(max_length=32)
    zona_postal = models.CharField('zona postal', max_length=4)

    class Meta:
        abstract = True


class InfoEmpleado(models.Model):
    '''
    Modelo que resguarda la información de un empleado
    '''
    profesion = models.ForeignKey(Profesion)
    cargo = models.CharField(max_length=128)
    dependencia = models.CharField(max_length=128)

    class Meta:
        abstract = True
