# -*- coding: utf-8 -*-
'''
Created on 26/04/2014

@author: rtorres
'''
from django.db import models

from proyectos_app.models.abstracts import PersonaSolicitante, Recupera
from proyectos_app.models.constantesModels import DATABASE_PRE


class UsuarioSolicitante(PersonaSolicitante, Recupera):
    """
    Modelo de usuario solicitante
    """
    usuario = models.IntegerField(unique=True, db_index=True)
    fecha_registro = models.DateTimeField('fecha de registro', auto_now=True, db_index=True)

    class Meta:
        verbose_name = 'usuario solicitante'
        verbose_name_plural = 'usuarios solicitantes'
        db_table = DATABASE_PRE + 'usuario_solicitante'

    def save(self, force_insert=False, force_update=False, using=None):
        self.respuesta_seguridad = self.respuesta_seguridad.strip().upper()
        PersonaSolicitante.save(self, force_insert=force_insert, force_update=force_update, using=using)

    def __unicode__(self):
        return self.nombre
