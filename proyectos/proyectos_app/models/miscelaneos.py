# -*- coding: utf-8 -*-
'''
Created on 06/05/2014

@author: rtorres
'''

from django.db import models
from django.template.defaultfilters import slugify

from proyectos_app.models.constantesModels import DATABASE_MISCELANEO


class Miscelaneo(models.Model):
    '''
    clase abstracta para guardar los miscelaneos configurables del sistema
    '''
    valor = models.CharField('valor', max_length=128, primary_key=True)
    descripcion = models.TextField('breve descripción', blank=True)
    creacion = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        abstract = True

    def __unicode__(self):
        return u"%s" % (self.valor)

    def save(self, *args, **kwargs):
        self.valor = self.valor.strip()
        return super(Miscelaneo, self).save(*args, **kwargs)

    @models.permalink
    def get_absolute_url(self):
        return (self.MISCELANEO_DETAIL, [self.slug()])

    @models.permalink
    def get_update_url(self):
        return (self.MISCELANEO_UPDATE, [self.slug()])

    @models.permalink
    def get_delete_url(self):
        return (self.MISCELANEO_DELETE, [self.slug()])

    @property
    def slug(self):
        return slugify(self.valor)


class Profesion(Miscelaneo):
    '''
    Profesiones que puede tener un empleado
    '''
    MISCELANEO_DETAIL = 'profesion_detail'
    MISCELANEO_DELETE = 'profesion_delete'
    MISCELANEO_UPDATE = 'profesion_update'

    class Meta(Miscelaneo.Meta):
        verbose_name_plural = 'profesión'
        verbose_name_plural = 'profesiones'
        db_table = DATABASE_MISCELANEO + 'profesion'
        abstract = False
