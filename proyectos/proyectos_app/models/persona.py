# -*- coding: utf-8 -*-
'''
Created on 03/05/2014

@author: rtores
'''

import datetime

from dateutil.relativedelta import relativedelta
from django.db import models

from comun.constantes import SEXO_OPCIONES, ESTADO_CIVIL_OPCIONES, AREA_OPCIONES, USUARIO_OPCIONES
from proyectos_app.models.abstracts import Persona, InformacionContacto, DireccionFiscal, \
    InfoEmpleado
from proyectos_app.models.constantesModels import DATABASE_PRE
from proyectos_app.models.usuarioSolicitante import UsuarioSolicitante


class PersonaNatural(Persona, InformacionContacto):
    '''
    Modelo de datos para la información de las personas naturales
    '''
    fecha_nacimiento = models.DateField('fecha de nacimiento')
    sexo = models.CharField(max_length=16, choices=SEXO_OPCIONES)
    estado_civil = models.CharField(max_length=16, choices=ESTADO_CIVIL_OPCIONES)
    fecha_registro = models.DateTimeField('fecha de registro', auto_now=True, db_index=True)

    class Meta:
        verbose_name = 'persona natural'
        verbose_name_plural = 'personas naturales'
        db_table = DATABASE_PRE + 'persona_natural'

    def __unicode__(self):
        return "%s-%s %s %s" % (self.tipo_cedula, self.cedula, self.nombre, self.apellido)

    @property
    def edad(self):
        retorno = None
        hoy = datetime.date.today()
        if self.fechaNacimiento:
            retorno = u"%s" % relativedelta(hoy, self.fechaNacimiento).years
        return retorno


class PersonaJuridica(InformacionContacto, DireccionFiscal):
    '''
    Modelo de datos para la información de las personas juridicas
    '''
    rif = models.CharField(max_length=12)
    razon_social = models.CharField('razón social', max_length=255)
    area = models.CharField(max_length=16, choices=AREA_OPCIONES)
    fecha_registro = models.DateTimeField('fecha de registro', auto_now=True, db_index=True)

    class Meta:
        verbose_name = 'persona juridica'
        verbose_name_plural = 'personas juridicas'
        db_table = DATABASE_PRE + 'persona_juridica'

    def __unicode__(self):
        return "%s %s %s" % (self.rif, self.razon_social)


class FichaUsuario(InfoEmpleado):
    '''
    Almacena la información del usuario registrado
    '''
    usuario = models.OneToOneField(UsuarioSolicitante, primary_key=True)
    tipo = models.CharField('tipo de usuario', max_length=16, choices=USUARIO_OPCIONES)
    empresa = models.ForeignKey(PersonaJuridica)
    persona = models.ForeignKey(PersonaNatural)
    fecha_registro = models.DateTimeField('fecha de registro', auto_now=True, db_index=True)

    class Meta:
        abstract = False
        verbose_name = 'ficha del usuario'
        verbose_name_plural = 'fichas de los usuarios'
        db_table = DATABASE_PRE + 'ficha_usuario'
        unique_together = (("usuario", "tipo"),)

    def __unicode__(self):
        return "%s:%s " % (self.usuario, self.tipo)
