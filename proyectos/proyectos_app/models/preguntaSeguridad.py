# -*- coding: utf-8 -*-
'''
Created on 26/04/2014

@author: rtorres
'''
from django.db import models

from proyectos_app.models.constantesModels import DATABASE_PRE


class PreguntaSeguridad(models.Model):
    '''
    Clase que guarda las preguntas de seguridad
    '''
    pregunta = models.CharField('pregunta de seguridad', max_length=255, unique=True)

    class Meta:
        verbose_name = 'pregunta de seguridad'
        verbose_name_plural = 'preguntas de seguridad'
        db_table = DATABASE_PRE + 'pregunta_salida'

    def __unicode__(self):
        return self.pregunta
