# -*- coding: utf-8 -*-
'''
Created on 05/05/2014

@author: rtorres
'''

GRUPO_PROYECTOS = 'GRUPO_PROYECTOS'

usuario_solicitante = 'usuario_solicitante'

MSG_FIN_SESSION = 'Ha finalizado su session en la aplicación.'
MSG_LOGIN_REQUERIDO = 'Necesita loguearse para acceder al sistema.'

TIPO_USUARIO = 'tipo_usuario'
